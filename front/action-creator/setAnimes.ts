import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';

export const SetAnimes = 'anime/set-animes';

export const setAnimes = (animes : INormalizedData<IAnime>) => {
    return {
        type : SetAnimes,
        payload : { animes }
    };
};