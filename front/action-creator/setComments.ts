import { IComment } from '../../back/src/interface/IComment';
import { INormalizedData } from '../../back/src/interface/INormalizedData';

export const SetComments = 'comment/set-comments';

export const setComments = (comments : INormalizedData<IComment>) => {
    return {
        type : SetComments,
        payload : { comments }
    };
};