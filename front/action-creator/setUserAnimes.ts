import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUserAnime } from '../../back/src/interface/IUserAnime';

export const SetUserAnimes = 'userAnime/set-userAnimes';

export const setUserAnimes = (userAnimes : INormalizedData<IUserAnime>) => {
    return {
        type : SetUserAnimes,
        payload : { userAnimes }
    };
};