import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';

export const SetEpisodes = 'episode/set-episodes';

export const setEpisodes = (episodes : INormalizedData<IEpisode>) => {
    return {
        type : SetEpisodes,
        payload : { episodes }
    };
};