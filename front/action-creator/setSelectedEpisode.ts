import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';

export const SetSelectedEpisode = 'episode/set-selected-episode';

export const setSelectedEpisode = (episode : INormalizedData<IEpisode>) => {
    return {
        type : SetSelectedEpisode,
        payload : { episode }
    };
};