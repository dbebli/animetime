import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IView } from '../../back/src/interface/IView';

export const SetViews = 'view/set-views';

export const setViews = (views : INormalizedData<IView>) => {
    return {
        type : SetViews,
        payload : { views }
    };
};