import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { IUser } from '../../../back/src/interface/IUser';

export const SetUsers = 'users/set-users';

export const setUsers = (users : INormalizedData<IUser>) => {
    return {
        type : SetUsers,
        payload : { users }
    };
};