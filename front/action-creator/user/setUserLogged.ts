import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { IUser } from '../../../back/src/interface/IUser';

export const SetUserLogged = 'user/set-userLogged';

export const setUserLogged = (userLogged : INormalizedData<IUser>) => {
    return {
        type : SetUserLogged,
        payload : { userLogged }
    };
};