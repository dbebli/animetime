import { StyleSheet } from 'react-native';

export const mainStyle = StyleSheet.create({
    boldText : {
        color : '#fff',
        fontSize : 20,
        fontWeight : 'bold'
    },
     basicText : {
        color : '#fff',
        fontSize : 14,
    },
    chartText : {
        color : '#1accf0',
        fontSize : 26,
        fontWeight : 'bold'
    },
})