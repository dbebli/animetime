import { postData } from '../helper/postData';
import Service, { ipAddress } from './Service';

const baseUrl = `http://${ipAddress}:3022/comment`;

class CommentService extends Service {
    constructor(baseUrl : string) {
        super(baseUrl);
    }

    async respond(body : any) {
        return await fetch(`${ baseUrl }/state`, postData(body));
    }

}

const CommentServiceServiceInstance = new CommentService(baseUrl);

export default CommentServiceServiceInstance;