import Service, { ipAddress } from './Service';

const baseUrl = `http://${ ipAddress }:3022/episode`;

const EpisodeService = new Service(baseUrl);

export default EpisodeService;