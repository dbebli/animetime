import { postData } from '../helper/postData';
import Service, { ipAddress } from './Service';

const baseUrl = `http://${ ipAddress }:3022/anime`;

class AnimeService extends Service {
    constructor(baseUrl : string) {
        super(baseUrl);
    }

    async addToList(body : any) {
        return await fetch(`${ baseUrl }/addToList`, postData(body));
    }

    async removeFromList(body : any) {
        return await fetch(`${ baseUrl }/removeFromList`, postData(body));
    }

}

const AnimeServiceInstance = new AnimeService(baseUrl);

export default AnimeServiceInstance;