import { postData } from '../helper/postData';
import Service, { ipAddress } from './Service';

const baseUrl = `http://${ipAddress}:3022/user`;

class UserService extends Service {
    constructor(baseUrl : string) {
        super(baseUrl);
    }

    public async create(body : any) {
        return await fetch(`${ this.baseUrl }/create`, postData(body));
    }

    async auth(body : any) {
        return await fetch(`${ baseUrl }/login`, postData(body));
    }

    async resetPassword(body : any) {
        return await fetch(`${ baseUrl }/passwordReset`, postData(body));
    }

}

const UserServiceInstance = new UserService(baseUrl);

export default UserServiceInstance;