import { postData } from '../helper/postData';
import Service, { ipAddress } from './Service';

const baseUrl = `http://${ ipAddress }:3022/view`;

const ViewService = new Service(baseUrl);

export default ViewService;