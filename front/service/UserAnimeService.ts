import Service, { ipAddress } from './Service';

const baseUrl = `http://${ipAddress}:3022/userAnime`;

const UserAnimeService = new Service(baseUrl);

export default UserAnimeService;