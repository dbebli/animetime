import { postData } from '../helper/postData';
import Constants from 'expo-constants';

const { manifest } = Constants;
export const ipAddress = manifest?.debuggerHost!.split(':').shift();
console.log(ipAddress);

export default class Service {
    protected baseUrl : string;

    constructor(baseUrl : string) {
        this.baseUrl = baseUrl;
    }

    public async create(body : any) {
        return await fetch(`${ this.baseUrl }`, postData(body));
    }

    public async findAll() {
        const init = {
            method : 'GET',
            headers : {
                'Content-Type' : 'application/json',
            }
        };
        return await fetch(`${ this.baseUrl }`, init);
    }

    public async findOne(id : number | string) {
        const init = {
            method : 'GET',
            headers : {
                'Content-Type' : 'application/json',
            }
        };
        return await fetch(`${ this.baseUrl }/${ id }`, init);
    }

    public async update(id : number | string, body : any) {
        const init = {
            method : 'PUT',
            headers : {
                'Content-Type' : 'application/json',
            },
            body : JSON.stringify(body)
        };
        return await fetch(`${ this.baseUrl }/${ id }`, init);
    }

    public async remove(id : number | string) {
        const init = {
            method : 'DELETE',
            headers : {
                'Content-Type' : 'application/json',
            }
        };
        return await fetch(`${ this.baseUrl }/${ id }`, init);
    }
}