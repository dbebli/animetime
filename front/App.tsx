import * as React from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './reducer';
import { SmartAnimeDetails } from './screen/AnimeDetails';
import { SmartAnimeToSee } from './screen/AnimeToSee';
import { SmartEpisodeComments } from './screen/EpisodeComments';
import { SmartHome } from './screen/Home';
import Login from './screen/Login';
import { SmartSearch } from './screen/Search';

export const BottomNavigator = createBottomTabNavigator(
    {
        Home : {
            screen : SmartHome,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => (
                    <Icon
                        name={ 'list' } color={ tintColor }
                        size={ 24 }/>)
            })
        },
        Search : {
            screen : SmartSearch,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => (
                    <Icon
                        name={ 'search' } color={ tintColor }
                        size={ 24 }/>)
            })
        },
        AnimeMonitoring : {
            screen : SmartAnimeToSee,
            navigationOptions : () => ({
                tabBarIcon : ({ tintColor }) => (
                    <Icon
                        name={ 'tv' } color={ tintColor }
                        size={ 24 }/>),
            })
        },
    },
    {
        tabBarOptions : {
            activeTintColor : '#1accf0',
            showLabel : false,
            activeBackgroundColor : '#242323',
            inactiveBackgroundColor : '#242323',
            showIcon : true
        }
    }
);
const AppNavigator = createStackNavigator(
    {
        Home : { screen : BottomNavigator, navigationOptions : { headerShown : false } },
        AnimeDetails : { screen : SmartAnimeDetails, navigationOptions : { headerShown : false } },
        Search : { screen : SmartSearch, navigationOptions : { headerShown : false } },
        AnimeMonitoring : { screen : SmartAnimeToSee, navigationOptions : { headerShown : false } },
        EpisodeComments : { screen : SmartEpisodeComments, navigationOptions : { headerShown : false } },
        Login : { screen : Login, navigationOptions : { headerShown : false } },
    },
    {
        initialRouteName : 'Login',
        defaultNavigationOptions : {
            cardStyle : { backgroundColor : '#242323' },
        }
    }
);

const AppContainer = createAppContainer(AppNavigator);
// const persistedReducer = persistReducer({
//     key : 'myAnime',
//     storage : storage,
// }, rootReducer);
// const store = createStore(persistedReducer);
// const persistor = persistStore(store);
// export default class App extends React.Component {
//     render() {
//         return (
//             <PopupProvider>
//                 <Provider store={ store }>
//                     <PersistGate loading={ null } persistor={ persistor }>
//                         <AppContainer/>
//                     </PersistGate>
//                 </Provider>
//             </PopupProvider>
//         )
//     }
// }

const store = createStore(rootReducer);

export default class App extends React.Component {
    public async componentDidMount() {

    }

    render() {
        return (
            <Provider store={ store }>
                <AppContainer/>
            </Provider>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor : '#000',
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
    },
});
