import * as React from 'react';
import { FlatList, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IComment } from '../../back/src/interface/IComment';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { Identified } from '../../back/src/type/Identified';
import { SmartComment } from '../component/Comment';
import { SmartCommentForm } from '../component/form/CommentForm';
import { Popup } from '../component/Popup';
import { all, filter, getEntity } from '../utils';

export interface IEpisodeCommentsProps {
    comments : Identified<IComment>[];
    userLogged : Identified<IUser>;
    episode : Identified<IEpisode>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export interface IEpisodesCommentsState {
    isModalVisible : boolean;
}

export default class EpisodeComments extends React.Component<IEpisodeCommentsProps, IEpisodesCommentsState> {
    constructor(props : IEpisodeCommentsProps) {
        super(props);
        this.state = {
            isModalVisible : false,
        }
    }

    render() {
        const { userLogged, navigation, comments } = this.props;
        const { isModalVisible } = this.state;
        return (
            <View style={ styles.container }>
                <TouchableOpacity onPress={ this.goBack } style={ styles.backButton }>
                    <View style={ styles.stats }>
                        <Icon name="arrow-left" size={ 24 } color='#fff'/>
                        <Text style={ styles.title }>Retour</Text>
                    </View>

                </TouchableOpacity>
                <Popup isVisible={ isModalVisible } hideModal={ this.hideModal } title="Ajouter un commentaire"
                       content={ <SmartCommentForm idEpisode={ this.props.episode.id } navigation={ navigation }/> }/>
                <Text style={ styles.titleEpisode }>{this.props.episode.episodeNumber} | {this.props.episode.title}</Text>
                <View style={ styles.stats }>
                    <Icon name='comment' color='#1accf0' size={ 24 } style={ { marginRight : 6 } }/>
                    <Text style={ styles.title }>{ comments.length } commentaire</Text>
                </View>

                <ScrollView style={ { width : '95%' } }>
                    <FlatList
                        data={ comments }
                        showsHorizontalScrollIndicator={ false }
                        keyExtractor={ item => String(item.id) }
                        renderItem={ ({ item }) => <SmartComment idComment={ item.id }/> }
                    />


                </ScrollView>
                <TouchableHighlight
                    style={ styles.addCommentButton }
                    onPress={ this.addComment
                    }
                >
                    <Icon name='comment' color="#242323" size={ 24 }/>
                </TouchableHighlight>

            </View>
        );
    }

    private hideModal = () => {
        this.setState({
            isModalVisible : false,
        })
    };

    private addComment = () => {
        this.setState({
            isModalVisible : true,
        })
    };

    private goBack = () => {
        this.props.navigation.goBack();
    }
}

interface IStoreState {
    userLogged : INormalizedData<IUser>;
    episodes : INormalizedData<IEpisode>;
    comments : INormalizedData<IComment>;
    selectedEpisode : INormalizedData<IEpisode>;
}

interface ISmartEpisodeCommentsProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartEpisodeCommentsProps) => {
    const userLogged = all(state.userLogged)[0];
    const idEpisode = all(state.selectedEpisode)[0].id;
    return {
        userLogged,
        episode : getEntity(state.episodes)(idEpisode),
        comments : filter(state.comments)(comment => comment.idEpisode === idEpisode),
    }
};

export const SmartEpisodeComments = connect(mapStateToProps)(EpisodeComments);

const styles = StyleSheet.create({
    container : {
        height : '100%',
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        marginTop : 24,
    },
    backButton : {
        position : 'absolute',
        left : 5,
        top : 0,
    },
    title : {
        fontSize : 18,
        color : '#fff',
    },
    titleEpisode : {
        fontSize : 18,
        color : '#fff',
        fontWeight : 'bold',
        marginTop: 35
    },
    stats : {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'center',
        marginHorizontal : 5,
        marginVertical : 5,
    },

    statsContainer : {
        backgroundColor : '#4a4848',
        alignItems : 'center',
        marginTop : 15,
        width : 100,
    },
    image : {
        borderRadius : 50,
        height : 120,
        width : 120,
    },
    addCommentButton : {
        display : 'flex',
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : '#1accf0',
        borderRadius : 20,
        width : 40,
        height : 40,
        position : 'absolute',
        right : 10,
        bottom : 60,
        elevation : 2
    },

});
