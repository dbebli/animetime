import * as React from 'react';
import { FlatList, ScrollView, StyleSheet, Text } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import BetweenTitle from '../component/BetweenTitle';
import { SmartEpisode } from '../component/Episode';
import { getAnimesEpisodeToWatch } from '../selector/getAnimesEpisodeToWatch';
import { getNotStartedAnimesFirstEpisode } from '../selector/getNotStartedAnimesFirstEpisode';
import { getUserAnimeList } from '../selector/getUserAnimeList';
import { all } from '../utils';

export interface IAnimeToSeeProps {
    toContinue : Identified<IEpisode>[];
    notStarted : Identified<IEpisode>[];
    userLogged : Identified<IUser>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class AnimeToSee extends React.Component<IAnimeToSeeProps> {
    constructor(props : IAnimeToSeeProps) {
        super(props);
        this.state = {}
    }

    componentDidMount() {

    }

    render() {
        const { userLogged, navigation, notStarted, toContinue } = this.props;
        return (
            <ScrollView style={ styles.container }>
                <BetweenTitle title="A voir"/>
                { toContinue.length ?
                    <FlatList
                        data={ toContinue }
                        showsHorizontalScrollIndicator={ false }
                        keyExtractor={ item => String(item.id) }
                        renderItem={ ({ item }) => <SmartEpisode navigation={ navigation } idEpisode={ item.id }/> }
                    /> : <Text style={ styles.text }>Vous etes à jour dans tout vos animés</Text> }

                <BetweenTitle title="Pas commencés"/>
                { notStarted.length ? <FlatList
                        data={ notStarted }
                        showsHorizontalScrollIndicator={ false }
                        keyExtractor={ item => String(item.id) }
                        renderItem={ ({ item }) => <SmartEpisode idEpisode={ item.id } navigation={ navigation }/> }
                    />
                    : <Text style={ styles.text }>Vous avez commencé tous les animés de votre liste</Text> }

            </ScrollView>
        );
    }
}

interface IStoreState {
    userLogged : INormalizedData<IUser>;
    userAnimes : INormalizedData<IUserAnime>;
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>
    views : INormalizedData<IView>
}

const mapStateToProps = (state : IStoreState) => {
    const userLogged = all(state.userLogged)[0];
    const userAnimes = getUserAnimeList(state)(userLogged.id);

    return {
        userLogged,
        toContinue : getAnimesEpisodeToWatch(state)(userAnimes),
        notStarted : getNotStartedAnimesFirstEpisode(state)(userAnimes)
    }
};

export const SmartAnimeToSee = connect(mapStateToProps)(AnimeToSee);

const styles = StyleSheet.create({
    container : {
        marginTop : 24,
    },
    text : {
        textAlign : 'center',
        fontSize : 16,
        color : '#fff',
        marginTop : 10,

    },
    statsContainer : {
        backgroundColor : '#4a4848',
        alignItems : 'center',
        marginTop : 15,
        width : 100,
    },
    image : {
        borderRadius : 50,
        height : 120,
        width : 120,
    },

});
