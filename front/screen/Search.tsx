import * as React from 'react';
import { Image, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { Identified } from '../../back/src/type/Identified';
import avatar from '../assets/defaultavatar.png';
import { formStyles } from '../component/form/LoginForm';
import { SmartSearchResults } from '../component/SearchResults';
import { SmartUserAnimeList } from '../component/UserAnimeList';
import { mainStyle } from '../styles/mainStyle';

export interface ISearchProps {
    userLogged : Identified<IUser>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

interface ISearchState {
    query : string;
}

export default class Search extends React.Component<ISearchProps, ISearchState> {
    constructor(props : ISearchProps) {
        super(props);
        this.state = {
            query : '',
        }
    }

    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };

    componentDidMount() {

    }

    render() {
        const { userLogged, navigation } = this.props;
        const { query } = this.state;
        return (
            <View style={ { marginTop : 24 } }>
                <TextInput multiline={ true } style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'query')
                } } value={ query } placeholder="Rechercher" placeholderTextColor="lightgrey"/>
                <SmartSearchResults query={ query } navigation={ navigation }/>
            </View>

        );
    }
}

interface IStoreState {
    userLogged : INormalizedData<IUser>;
}

const mapStateToProps = (state : IStoreState) => {
    const userLoggedFake : Identified<IUser> = {
        createdAt : '05//06/2020',
        email : 'dani@gmail.com',
        id : 1,
        password : 'odakl,',
        username : 'dani'

    };
    return {
        // userLogged : all(state.userLogged)[0] || undefined,
        userLogged : userLoggedFake,
    }
};

export const SmartSearch = connect(mapStateToProps)(Search);

const styles = StyleSheet.create({
    container : {
        marginTop : 24,
        display : 'flex',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#242323',
        marginBottom : 20,
        padding : 5,
        height : 'auto',
    },
    statsContainer : {
        backgroundColor : '#4a4848',
        alignItems : 'center',
        marginTop : 15,
        width : 100,
    },
    image : {
        borderRadius : 50,
        height : 120,
        width : 120,
    },

});
