import * as React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import avatar from '../assets/defaultavatar.png';
import { SmartAnimeForm } from '../component/form/AnimeForm';
import { Popup } from '../component/Popup';
import { SmartUserAnimeList } from '../component/UserAnimeList';
import { mainStyle } from '../styles/mainStyle';
import { all, filter } from '../utils';

export interface IHomeProps {
    userLogged : Identified<IUser>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    totalView : number;
}

export interface IHomeState {
    isModalVisible : boolean;
}

export default class Home extends React.Component<IHomeProps, IHomeState> {
    constructor(props : IHomeProps) {
        super(props);
        this.state = {
            isModalVisible : false,
        }
    }

    async componentDidMount() {

    }

    render() {
        const { userLogged, navigation, totalView } = this.props;
        const { isModalVisible } = this.state;
        return (
            <ScrollView>
                <View style={ styles.container }>
                    <Popup isVisible={ isModalVisible } hideModal={ this.hideModal } title="Ajout d\'un animé"
                           content={ <SmartAnimeForm navigation={ navigation }/> }/>
                    { userLogged.avatar && userLogged.avatar !== '' ?
                        <Image source={ { uri : userLogged.avatar } } style={ styles.image } resizeMode="cover"/> :
                        <Image source={ avatar } style={ styles.image } resizeMode={ 'cover' }/>
                    }
                    <Text style={ mainStyle.boldText }>{ userLogged.username }</Text>
                    <View style={ styles.statsContainer }>
                        <View style={ styles.stats }>
                            <Text style={ mainStyle.basicText }>episodes vus</Text>
                            <Text style={ mainStyle.chartText }>{ totalView }</Text>
                        </View>
                        <TouchableOpacity style={ styles.stats } onPress={ this.createAnime }>
                            <Icon name='plus' color='#1accf0' size={ 24 } style={ { marginRight : 6 } }/>
                            <Text style={ mainStyle.basicText }>Ajouter anime</Text>
                        </TouchableOpacity>
                    </View>
                    <SmartUserAnimeList navigation={ navigation } idUser={ userLogged.id }/>
                </View>
            </ScrollView>
        );
    }

    private hideModal = () => {
        this.setState({
            isModalVisible : false,
        })
    };

    private createAnime = () => {
        this.setState({
            isModalVisible : true,
        })
    }
}

interface IStoreState {
    userLogged : INormalizedData<IUser>;
    views : INormalizedData<IView>;
}

const mapStateToProps = (state : IStoreState) => {
    const userLogged = all(state.userLogged)[0];
    return {
        userLogged,
        totalView : filter(state.views)(view => view.idUser === userLogged.id && view.state === 'S').length
    }
};

export const SmartHome = connect(mapStateToProps)(Home);

const styles = StyleSheet.create({
    container : {
        marginTop : 24,
        display : 'flex',
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#242323',
        marginBottom : 20,
        padding : 5,
        height : 'auto',
    },
    statsContainer : {
        display : 'flex',
        flexDirection : 'row',
    },
    stats : {
        backgroundColor : '#4a4848',
        borderRadius : 10,
        alignItems : 'center',
        justifyContent : 'center',
        marginTop : 15,
        marginHorizontal : 5,
        width : 100,
    },
    image : {
        borderRadius : 50,
        height : 120,
        width : 120,
    },

});
