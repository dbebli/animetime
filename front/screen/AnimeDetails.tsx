import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import * as React from 'react';
import { Image, Route, StyleSheet } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { Identified } from '../../back/src/type/Identified';
import defaultCover from '../assets/defaultCover.png';
import { SmartAboutAnime } from '../component/AboutAnime';
import AnimeDetailsStackNavigator from '../navigation/AnimeDetailsStackNavigator';
import { get } from '../utils';

const Tab = createMaterialTopTabNavigator();

interface IAnimeDetailsProps {
    anime? : Identified<IAnime>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class AnimeDetails extends React.PureComponent<IAnimeDetailsProps> {
    public componentDidUpdate(prevProps : Readonly<IAnimeDetailsProps>, prevState : Readonly<{}>, snapshot? : any) : void {
        if (!this.props.anime) {
            this.props.navigation.navigate('Home');
        }
    }

    public componentDidMount() : void {
        if (!this.props.anime) {
            this.props.navigation.navigate('Home');
        }
    }

    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { anime, navigation } = this.props;
        return (
            <NavigationContainer independent={ true }>
                { anime?.cover ? <Image source={ { uri : anime.cover } } style={ styles.headerImage } resizeMode="cover"/> :
                    <Image source={ defaultCover } style={ styles.headerImage }
                           resizeMode={ 'cover' }/>
                }

                <Tab.Navigator
                    sceneContainerStyle={ { backgroundColor : '#242323' } }
                    tabBarOptions={ {
                        labelStyle : { color : '#fff' },
                        indicatorStyle : { height : 5, backgroundColor : '#1accf0' },
                        style : { backgroundColor : '#242323' },
                    } }>
                    <Tab.Screen name="AnimeDetails" options={ { tabBarLabel : 'A propos' } } initialParams={ { idAnime : anime?.id } }
                                component={ SmartAboutAnime }/>
                    <Tab.Screen name="Episode" initialParams={ { idAnime : anime?.id } } component={ AnimeDetailsStackNavigator }/>
                </Tab.Navigator>
            </NavigationContainer>
        )
    }

}

export interface ISmartAnimeDetailsProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    route : Route;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartAnimeDetailsProps) => {
    const idAnime = ownProps.navigation.state.params ? ownProps.navigation.state.params.idAnime : ownProps.route.params.idAnime;
    return {
        anime : get(state.animes)(idAnime) || undefined,
    }
};

export const SmartAnimeDetails = connect(mapStateToProps)(AnimeDetails);

const styles = StyleSheet.create({
    body : {
        display : 'flex',
        justifyContent : 'center',
        height : 150,
        padding : 5,
        borderBottomEndRadius : 20,
        borderBottomStartRadius : 20,
    },
    containerHorizontal : {
        width : 250,
    },
    container : {
        display : 'flex',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        borderBottomEndRadius : 20,
        borderBottomStartRadius : 20,
        marginBottom : 20,
        marginHorizontal : 10,
        height : 'auto',
        shadowColor : '#000',
        shadowOffset : {
            width : 0,
            height : 10,
        },
        shadowOpacity : 0.08,
        shadowRadius : 20,
        elevation : 5,
        // borderWidth:1,
        // borderColor:"red",
    },
    title : {
        fontSize : 18,
        fontWeight : 'bold'
    },
    dateBox : {
        position : 'absolute',
        left : 10,
        top : -30,
    },
    subtitle : {
        fontSize : 15,
        color : '#B0B0B0'
    },
    headerImage : {
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        height : 200,
        width : '100%',
    }
});
