import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import * as React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { SmartLoginForm } from '../component/form/LoginForm';
import { SmartSubscriptionForm } from '../component/form/SubscriptionForm';

interface ILoginProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

interface ILoginState {
    showLoginForm : boolean
}

export default class Login extends React.PureComponent<ILoginProps, ILoginState> {
    public state = {
        showLoginForm : true
    };

    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { showLoginForm } = this.state;
        const { navigation } = this.props;
        const screenLabel = showLoginForm ? 'Aller vers Inscription >' : ' Aller vers Connexion >';
        return (
            <View style={ { width : '100%', marginTop : 24, display : 'flex', alignItems : 'center', justifyContent : 'center' } }>
                <View style={ { width : '90%' } }>
                    <TouchableOpacity onPress={ this.changeScreen }>
                        <Text style={ { color : '#fff', textAlign : 'center', textTransform : 'uppercase' } }>{ screenLabel }</Text>
                    </TouchableOpacity>

                    {
                        showLoginForm ? <SmartLoginForm navigation={ navigation }/> : <SmartSubscriptionForm navigation={ navigation }/>
                    }
                </View>

            </View>
        )
    }

    private changeScreen = () => {
        this.setState({
            showLoginForm : !this.state.showLoginForm
        })
    }
}


