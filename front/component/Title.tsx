import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface ITitleProps {
    title : string;
}

export default class Title extends React.PureComponent <ITitleProps> {
    constructor(props : ITitleProps) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
    }

    render() {
        const { title } = this.props;
        return (
            <Text style={ styles.title }>{ title }</Text>
        );
    }
}

const styles = StyleSheet.create({
    title : {
        fontSize : 28,
        color : '#FFF',
        fontWeight : 'bold',
        marginVertical : 15,
        textAlign:'center',
    },
});

