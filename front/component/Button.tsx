import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as React from 'react';
import { GestureResponderEvent, StyleProp, StyleSheet, Text, TouchableOpacity, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export interface IButtonProps {
    className? : StyleProp<ViewStyle>;
    preventSubmit? : boolean;
    label? : string;
    disabled? : boolean;
    onPress : (...args : any[]) => any;
    onDisabledClick? : (...args : any[]) => any;
    icon? : string;
    size? : number;
    color? : string;
}

export class Button extends React.PureComponent<IButtonProps> {

    public render() {
        const { label, icon, color, size } = this.props;
        let classNames = this._getBaseClassNames();
        let iconTag,
            labelTag;

        if (icon) {
            iconTag = <Icon name={ icon } color={ color } size={ size }/>;
        }

        if (label) {
            labelTag = <Text style={ { color : '#fff', textTransform : 'uppercase' } }>{ label }</Text>

        }

        let buttonOptionalProps : { type? : 'button' | 'reset' | 'submit' } = this.props.preventSubmit ? { type : 'button' } : {};
        const separation = icon && label ? <Text>|</Text> : null;
        return (
            <TouchableOpacity { ...buttonOptionalProps } onPress={ this._onPress } style={ classNames }>
                { iconTag }
                { separation }
                { labelTag }
            </TouchableOpacity>
        );
    }

    private _onPress = (event : GestureResponderEvent) => {
        event.stopPropagation();
        if (!this.props.disabled) {
            this.props.onPress();
        } else if (this.props.onDisabledClick) {
            this.props.onDisabledClick();
        }
    };

    private _getBaseClassNames() {
        const { className } = this.props;
        const classNames : StyleProp<ViewStyle>[] = [];

        if (className) {
            classNames.push(className);
        }

        return classNames;
    }

}

export const ButtonStyles = StyleSheet.create({
    btnPrimary : {
        backgroundColor : '#1accf0',
    },

    btnValidation : {
        backgroundColor : '#2CFF7B',
    },
    btnWarning : {
        backgroundColor : '#FFA922',
    },

    btnDanger : {
        backgroundColor : '#FF1227',
    },

    btn : {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'center',
        borderRadius : 4,
        fontSize : 12,
        width : '100%',
        height : 40,
        margin : 4,
        padding : 5,
        color : '#fff',
    }
});
