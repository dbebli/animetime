import * as React from 'react';
import { FlatList, Route, ScrollView, StyleSheet } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { Identified } from '../../back/src/type/Identified';
import { filter, get } from '../utils';
import { SmartEpisode } from './Episode';

interface IAnimeEpisodesProps {
    episodes : Identified<IEpisode>[];
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class AnimeEpisodes extends React.PureComponent<IAnimeEpisodesProps> {
    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { episodes, navigation } = this.props;
        return (
            <ScrollView>
                <FlatList
                    data={ episodes }
                    showsHorizontalScrollIndicator={ false }
                    keyExtractor={ item => String(item.id) }
                    renderItem={ ({ item }) => <SmartEpisode deletetable={ true } navigation={ navigation } idEpisode={ item.id }/> }
                />
            </ScrollView>
        )
    }

}

export interface ISmartAnimeEpisodesProps {
    route : Route;
}

interface IStoreState {
    episodes : INormalizedData<IEpisode>;
    animes : INormalizedData<IAnime>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartAnimeEpisodesProps) => {
    const idAnime = ownProps.route.params.idAnime;
    const animeExist = get(state.animes)(idAnime) || undefined;
    return {
        episodes : animeExist ? filter(state.episodes)(episode => episode.idAnime === idAnime) : [],
    }
};

export const SmartAnimeEpisodes = connect(mapStateToProps)(AnimeEpisodes);

const styles = StyleSheet.create({
    body : {
        display : 'flex',
        justifyContent : 'center',
        height : 150,
        padding : 5,
        backgroundColor : '#fff',
        borderBottomEndRadius : 20,
        borderBottomStartRadius : 20,
    },
    containerHorizontal : {
        width : 250,
    },
    container : {
        display : 'flex',
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        borderBottomEndRadius : 20,
        borderBottomStartRadius : 20,
        marginBottom : 20,
        marginHorizontal : 10,
        height : 'auto',
        shadowColor : '#000',
        shadowOffset : {
            width : 0,
            height : 10,
        },
        shadowOpacity : 0.08,
        shadowRadius : 20,
        elevation : 5,
        // borderWidth:1,
        // borderColor:"red",
    },
    title : {
        fontSize : 18,
        fontWeight : 'bold'
    },
    dateBox : {
        position : 'absolute',
        left : 10,
        top : -30,
    },
    subtitle : {
        fontSize : 15,
        color : '#B0B0B0'
    },
    headerImage : {
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        height : 120,
        width : '100%',
    }
});
