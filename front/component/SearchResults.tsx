import * as React from 'react';
import { FlatList, ScrollView, StyleSheet, Text, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { Identified } from '../../back/src/type/Identified';
import { all } from '../utils';
import { SmartAnime } from './Anime';

export interface ISearchResultsProps {
    animes : Identified<IAnime>[];
    query : string;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

interface ISearchResultsState {
    animes : Identified<IAnime>[];
    query : string;
}

export default class SearchResults extends React.PureComponent<ISearchResultsProps, ISearchResultsState> {
    constructor(props : ISearchResultsProps) {
        super(props);
        this.state = {
            animes : this.props.animes,
            query : '',
        }
    }

    public componentDidUpdate(prevProps : Readonly<ISearchResultsProps>, prevState : Readonly<ISearchResultsState>, snapshot? : any) : void {
        this.setState({
            query : prevProps.query !== this.props.query ? this.props.query : this.state.query,
            animes : prevProps.query !== this.props.query || prevProps.animes !== this.props.animes  ? this.props.animes.filter(
                anime => anime.title.toLowerCase().search(this.props.query.toLowerCase()) !== -1) : this.state.animes,
        });
    }

    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { navigation, query } = this.props;
        const { animes } = this.state;
        return (
            <ScrollView>
                <View style={ styles.container }>
                    { animes.length ? <>
                            <FlatList
                                data={ animes }
                                numColumns={ 3 }
                                showsHorizontalScrollIndicator={ false }
                                keyExtractor={ item => String(item.id) }
                                renderItem={ ({ item }) => <SmartAnime navigation={ navigation } idAnime={ item.id }/> }
                            />
                        </>
                        : <Text style={ styles.text }>Aucun résultats</Text>
                    }
                </View>


            </ScrollView>
        )
    }

}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    userAnimes : INormalizedData<IUserAnime>;
}

const mapStateToProps = (state : IStoreState) => {
    return {
        animes : all(state.animes),
    }
};

export const SmartSearchResults = connect(mapStateToProps)(SearchResults);

const styles = StyleSheet.create({

    container : {
        display : 'flex',
        alignItems : 'center',
        justifyContent : 'center',
    },
    title : {
        fontSize : 18,
        fontWeight : 'bold'
    },
    text : {
        fontSize : 18,
        marginTop : 20,
        color : '#fff',
        textAlign : 'center',
    },

    headerImage : {
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        height : 120,
        width : '100%',
    }
});
