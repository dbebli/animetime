import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { Identified } from '../../back/src/type/Identified';
import defaultCover from '../assets/defaultCover.png';
import { get, getEntity } from '../utils';

interface IAnimeProps {
    anime : Identified<IAnime>;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class Anime extends React.PureComponent<IAnimeProps> {
    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { anime } = this.props;
        return (
            <View>
                { anime ?
                    <TouchableOpacity onPress={ this.seeAnimeDetails }>
                        { anime.cover ? <Image source={ { uri : anime.cover } } style={ styles.headerImage } resizeMode="cover"/> :
                            <Image source={ defaultCover } style={ styles.headerImage }
                                   resizeMode={ 'cover' }/>
                        }

                    </TouchableOpacity> : null
                }

            </View>
        )
    }

    private seeAnimeDetails = () => {
        this.props.navigation.navigate('AnimeDetails', { idAnime : this.props.anime.id });
    }
}

export interface ISmartAnimeProps {
    idAnime : number;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartAnimeProps) => {
    return {
        anime : get(state.animes)(ownProps.idAnime) || undefined
    }
};

export const SmartAnime = connect(mapStateToProps)(Anime);

const styles = StyleSheet.create({

    title : {
        fontSize : 14,
        textAlign : 'center',
        color : '#fff',
        fontWeight : 'bold'
    },

    headerImage : {
        backgroundColor : 'red',
        marginHorizontal : 10,
        overflow : 'hidden',
        marginVertical : 10,
        height : 120,
        width : 100,
    }
});
