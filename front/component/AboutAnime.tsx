import * as React from 'react';
import { Route, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { Identified } from '../../back/src/type/Identified';
import { setAnimes } from '../action-creator/setAnimes';
import { setUserAnimes } from '../action-creator/setUserAnimes';
import { normalize } from '../normalize';
import { getAnimeFollowerNumber } from '../selector/getAnimeFollowerNumber';
import { getUserAnimeList } from '../selector/getUserAnimeList';
import AnimeServiceInstance from '../service/AnimeService';
import UserAnimeService from '../service/UserAnimeService';
import { all, get } from '../utils';
import { Button, ButtonStyles } from './Button';
import { SmartEpisodeForm } from './form/EpisodeForm';
import { Popup } from './Popup';

interface IAboutAnimeProps {
    anime? : Identified<IAnime>;
    setAnimes : (animes : INormalizedData<IAnime>) => any;
    setUserAnimes : (userAnimes : INormalizedData<IUserAnime>) => any;
    userLogged : Identified<IUser>;
    followerNumber : number;
    isInList : boolean;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export interface IAboutAnimeState {
    isModalVisible : boolean;
}

export default class AboutAnime extends React.PureComponent<IAboutAnimeProps, IAboutAnimeState> {
    constructor(props : IAboutAnimeProps) {
        super(props);
        this.state = {
            isModalVisible : false,
        }
    }

    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { anime, navigation, isInList, followerNumber } = this.props;
        const { isModalVisible } = this.state;
        const addToListButton = isInList ? <Button icon="minus" color="#fff" size={ 18 } label="retirer de ma liste" onPress={ this.removeFromList }
                                                   className={ [ButtonStyles.btn, ButtonStyles.btn] }/>
            : <Button icon="plus" color="#fff" size={ 18 } label="ajouter a ma liste" onPress={ this.addTolist }
                      className={ [ButtonStyles.btn, ButtonStyles.btn] }/>
        return (
            <View>
                { anime ?
                    <>
                        <Popup isVisible={ isModalVisible } hideModal={ this.hideModal } title="Ajouter un commentaire"
                               content={ <SmartEpisodeForm idAnime={ anime.id } navigation={ navigation }/> }/>
                        <Text style={ styles.mainTitle }>{ anime.title }</Text>
                        <View style={ styles.block }>
                            <Text style={ styles.title }>Résumé</Text>
                            <Text style={ styles.date }>Année de sortie: { anime.yearReleased }</Text>
                            <Text style={ styles.description }>{ anime.description }</Text>
                        </View>

                        <View style={ styles.block }>
                            <View style={ styles.stats }>

                                <View style={ styles.stats }>
                                    <Icon name='calendar' color='#1accf0' size={ 24 } style={ { marginRight : 6 } }/>
                                    <Text style={ styles.title }>{ anime.releaseDay } | { anime.releaseHour }h</Text>
                                </View>
                                <View style={ styles.stats }>
                                    <Icon name='hourglass-start' color='#1accf0' size={ 24 } style={ { marginRight : 6 } }/>
                                    <Text style={ styles.title }>{ anime.episodeDuration } min</Text>
                                </View>
                            </View>
                            <View style={ styles.stats }>
                                <Icon name='users' color='#1accf0' size={ 24 } style={ { marginRight : 6 } }/>
                                <Text style={ styles.title }>Anime suivi par { followerNumber} personnes</Text>
                            </View>
                        </View>
                        { addToListButton }
                        <Button icon="plus" color="#fff" size={ 18 } label="ajouter episode" onPress={ this.addEpisode }
                                className={ [ButtonStyles.btn, ButtonStyles.btn] }/>
                        <Button icon="trash" color="#fff" size={ 18 } label="supprimer" onPress={ this.deleteAnime }
                                className={ [ButtonStyles.btn, ButtonStyles.btn] }/>

                    </>

                    : null }


            </View>
        )
    }

    private setReduxState = async () => {
        const getAnimes = await AnimeServiceInstance.findAll();
        const dataAnimes = await getAnimes.json();
        this.props.setAnimes(normalize(dataAnimes.animes));
        const getUserAnimes = await UserAnimeService.findAll();
        const dataUserAnimes = await getUserAnimes.json();
        this.props.setUserAnimes(normalize(dataUserAnimes.userAnimes));
    };

    private hideModal = () => {
        this.setState({
            isModalVisible : false,
        })
    };

    private addTolist = async () => {
        await AnimeServiceInstance.addToList({ idUser : this.props.userLogged.id, idAnime : this.props.anime?.id });
        await this.setReduxState();

    };
    private removeFromList = async () => {
        await AnimeServiceInstance.removeFromList({ idUser : this.props.userLogged.id, idAnime : this.props.anime?.id });
        await this.setReduxState();
    };

    private addEpisode = () => {
        this.setState({
            isModalVisible : true,
        })
    };

    private deleteAnime = async () => {
        await AnimeServiceInstance.remove(this.props.anime!.id);
        await this.setReduxState();
    }
}

export interface ISmartAboutAnimeProps {
    route : Route;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
    userAnimes : INormalizedData<IUserAnime>;
    userLogged : INormalizedData<IUser>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartAboutAnimeProps) => {
    const idAnime = ownProps.route.params.idAnime;
    const userLogged = all(state.userLogged)[0];
    const anime = get(state.animes)(idAnime) || undefined;
    const userAnimeList = idAnime ? getUserAnimeList(state)(userLogged.id) : [];
    const userAnimeId = userAnimeList.map(anime => anime.id);
    return {
        userLogged,
        anime,
        followerNumber : anime ? getAnimeFollowerNumber(state)(anime.id) : 0,
        isInList : userAnimeId.length && anime ? userAnimeId.includes(idAnime) : false,
    }
};
const mapDispatchToProps = (dispatch : Dispatch) => ({
    setAnimes : (animes : INormalizedData<IAnime>) => dispatch(setAnimes(animes)),
    setUserAnimes : (userAnimes : INormalizedData<IUserAnime>) => dispatch(setUserAnimes(userAnimes)),
});

export const SmartAboutAnime = connect(mapStateToProps, mapDispatchToProps)(AboutAnime);

const styles = StyleSheet.create({
    title : {
        fontSize : 18,
        color : '#fff',
    },
    mainTitle : {
        fontSize : 22,
        color : '#fff',
        fontWeight : 'bold',
        textAlign : 'center',
        marginTop : 10,
    },
    stats : {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        marginHorizontal : 5,
        marginVertical : 5,
    },
    description : {
        fontSize : 14,
        color : '#fff',
    },
    date : {
        fontSize : 15,
        color : '#a6a6a6',
    },
    block : {
        padding : 5,
        borderBottomWidth : 1,
        borderColor : '#4a4848',
    },
});
