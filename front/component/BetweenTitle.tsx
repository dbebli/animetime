import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface IBetweenProps {
    title : string;
}

export default class BetweenTitle extends React.PureComponent <IBetweenProps> {
    constructor(props : IBetweenProps) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
    }

    render() {
        const { title } = this.props;
        return (
            <View style={ styles.container }>
                <Text style={ styles.title }>{ title }</Text>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    title : {
        fontSize : 14,
        color : '#1accf0',
        textTransform : 'uppercase',
        fontWeight : 'bold',
    },
    container : {
        display : 'flex',
        marginTop : 10,
        marginHorizontal : 60,
        alignItems : 'center',
        backgroundColor : '#4a4848',
        borderRadius : 10
    },
});

