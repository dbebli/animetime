import * as React from 'react';
import { View, StyleSheet, Image, TouchableOpacity, FlatList, ScrollView, Text } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { Identified } from '../../back/src/type/Identified';
import { getUserAnimeList } from '../selector/getUserAnimeList';
import { filter, getEntity } from '../utils';
import { SmartAnime } from './Anime';
import Title from './Title';

export interface IUserAnimeListProps {
    animes : Identified<IAnime>[];
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class UserAnimeList extends React.PureComponent<IUserAnimeListProps> {
    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { animes, navigation } = this.props;
        return (
            <ScrollView style={ styles.container }>
                { animes.length ? <>
                        <Title title="Ma liste"/>
                        <FlatList
                            data={ animes }
                            numColumns={ 3 }
                            showsHorizontalScrollIndicator={ false }
                            keyExtractor={ item => String(item.id) }
                            renderItem={ ({ item }) => <SmartAnime navigation={ navigation } idAnime={ item.id }/> }
                        />
                    </>
                    : <TouchableOpacity onPress={ this.search }>
                        <Text style={ styles.text }>Rechercher</Text>
                    </TouchableOpacity>
                }

            </ScrollView>
        )
    }

    private search = () => {
        this.props.navigation.navigate('Search');
    }
}

export interface ISmartUserAnimeListProps {
    idUser : number;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    userAnimes : INormalizedData<IUserAnime>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartUserAnimeListProps) => {
    return {
        animes : getUserAnimeList(state)(ownProps.idUser)
    }
};

export const SmartUserAnimeList = connect(mapStateToProps)(UserAnimeList);

const styles = StyleSheet.create({

    container : {
        display : 'flex',
    },
    title : {
        fontSize : 18,
        fontWeight : 'bold'
    },
    text : {
        fontSize : 18,
        marginTop : 20,
        color : '#fff',
    },

    headerImage : {
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        height : 120,
        width : '100%',
    }
});
