import * as React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../../back/src/interface/IAnime';
import { IComment } from '../../../back/src/interface/IComment';
import { IEpisode } from '../../../back/src/interface/IEpisode';
import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { IUser } from '../../../back/src/interface/IUser';
import { IUserAnime } from '../../../back/src/interface/IUserAnime';
import { IView } from '../../../back/src/interface/IView';
import { setAnimes } from '../../action-creator/setAnimes';
import { setComments } from '../../action-creator/setComments';
import { setEpisodes } from '../../action-creator/setEpisodes';
import { setUserAnimes } from '../../action-creator/setUserAnimes';
import { setViews } from '../../action-creator/setViews';
import { setUserLogged } from '../../action-creator/user/setUserLogged';
import { setUsers } from '../../action-creator/user/setUsers';
import AnimeServiceInstance from '../../service/AnimeService';
import CommentServiceServiceInstance from '../../service/CommentService';
import EpisodeService from '../../service/EpisodeService';
import UserAnimeService from '../../service/UserAnimeService';
import ViewService from '../../service/ViewService';
import { Button, ButtonStyles } from '../Button';
import { normalize } from '../../normalize';
import UserServiceInstance from '../../service/UserService';
import Home from '../../screen/Home';

interface ILoginFormProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    setUserLogged : (user : INormalizedData<IUser>) => any;
    setUsers : (users : INormalizedData<IUser>) => any;
    setAnimes : (animes : INormalizedData<IAnime>) => any;
    setComments : (comments : INormalizedData<IComment>) => any;
    setEpisodes : (episodes : INormalizedData<IEpisode>) => any;
    setViews : (views : INormalizedData<IView>) => any;
    setUserAnime : (userAnimes : INormalizedData<IUserAnime>) => any;
}

interface ILoginFormState {
    username : string;
    password : string;
}

export default class LoginForm extends React.PureComponent<ILoginFormProps, ILoginFormState> {
    public state = {
        username : '',
        password : '',
    };

    componentDidMount() {

    }

    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };

    loadData = async () => {
        const getAnimes = await AnimeServiceInstance.findAll();
        const dataAnimes = await getAnimes.json();
        this.props.setAnimes(normalize(dataAnimes.animes));

        const getUsers = await UserServiceInstance.findAll();
        const dataUsers = await getUsers.json();
        this.props.setUsers(normalize(dataUsers.users));

        const getComments = await CommentServiceServiceInstance.findAll();
        const dataComments = await getComments.json();
        this.props.setComments(normalize(dataComments.comments));

        const getEpisodes = await EpisodeService.findAll();
        const dataEpisodes = await getEpisodes.json();
        this.props.setEpisodes(normalize(dataEpisodes.episodes));

        const getViews = await ViewService.findAll();
        const dataViews = await getViews.json();
        this.props.setViews(normalize(dataViews.views));

        const getUserAnimes = await UserAnimeService.findAll();
        const dataUserAnimes = await getUserAnimes.json();
        this.props.setUserAnime(normalize(dataUserAnimes.userAnimes));
    }
    ;
    submit = async (e : any) => {
        const response = await UserServiceInstance.auth(this.state);
        const data = await response.json();
        if (data.user) {
            this.props.setUserLogged(normalize(data.user));
            await this.loadData();
            this.props.navigation.navigate('Home');
        }
    };

    render() {
        const { password, username } = this.state;
        return (
            <View style={ formStyles.container }>
                <TextInput style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'username')
                } } placeholder={ 'nom d\'utilisateur ou email' } />
                <TextInput style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'password')
                } } placeholder={ 'mdp' }  secureTextEntry={ true }/>
                <Button label="se connecter" onPress={ this.submit } className={ [ButtonStyles.btn, ButtonStyles.btnPrimary] }/>
            </View>
        );
    }
}
;

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setUserLogged : (user : INormalizedData<IUser>) => dispatch(setUserLogged(user)),
    setUsers : (users : INormalizedData<IUser>) => dispatch(setUsers(users)),
    setAnimes : (animes : INormalizedData<IAnime>) => dispatch(setAnimes(animes)),
    setComments : (comments : INormalizedData<IComment>) => dispatch(setComments(comments)),
    setEpisodes : (episodes : INormalizedData<IEpisode>) => dispatch(setEpisodes(episodes)),
    setViews : (views : INormalizedData<IView>) => dispatch(setViews(views)),
    setUserAnime : (userAnimes : INormalizedData<IUserAnime>) => dispatch(setUserAnimes(userAnimes)),

});

export const SmartLoginForm = connect(undefined, mapDispatchToProps)(LoginForm);

export const formStyles = StyleSheet.create({
    container : {
        marginTop : 24,
        flex : 1,
        marginBottom : 20,
        padding : 5,
        height : 'auto',
    },
    input : {
        fontSize : 18,
        borderWidth : 1,
        height : 40,
        width : '90%',
        padding : 4,
        color : '#fff',
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
    },
});
