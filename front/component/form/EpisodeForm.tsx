import * as React from 'react';
import { StyleSheet, TextInput, View, Keyboard } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../../back/src/interface/IAnime';
import { IEpisode } from '../../../back/src/interface/IEpisode';
import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { Identified } from '../../../back/src/type/Identified';
import { setEpisodes } from '../../action-creator/setEpisodes';
import { normalize } from '../../normalize';
import { getAnimeLastEpisodeNumber } from '../../selector/getAnimeLastEpisodeNumber';
import EpisodeServiceServiceInstance from '../../service/EpisodeService';
import { getEntity } from '../../utils';
import { Button, ButtonStyles } from '../Button';
import FileInput from './FileInput';
import { formStyles } from './LoginForm';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

interface IEpisodeFormProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    anime : Identified<IAnime>;
    nextEpisodeNumber : number;
    setEpisodes : (comments : INormalizedData<IEpisode>) => any;
}

interface IEpisodeFormState {
    cover? : string | ArrayBuffer | null;
    title : string;
    description : string;
}

export default class EpisodeForm extends React.PureComponent<IEpisodeFormProps, IEpisodeFormState> {
    public state = {
        cover : undefined,
        title : '',
        description : '',
    };


    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };
     onPhotoSelected = (uri? : string | ArrayBuffer | null) => {
        this.setState({ cover : uri })
    };
    private setReduxState = async () => {
        const getEpisodes = await EpisodeServiceServiceInstance.findAll();
        const dataEpisodes = await getEpisodes.json();
        this.props.setEpisodes(normalize(dataEpisodes.episodes));
    };
    submit = async (e : any) => {
        const episode : IEpisode = {
            cover : this.state.cover,
            description : this.state.description,
            episodeNumber : this.props.nextEpisodeNumber,
            idAnime : this.props.anime.id,
            title : this.state.title
        };
        const response = await EpisodeServiceServiceInstance.create(episode);
        const data = await response.json();
        if (data.episode) {
            await this.setReduxState();
            this.setState({
                cover : undefined,
                title : '',
                description : '',
            })
        }

    };


    render() {
        const { description, cover, title } = this.state;
        return (
            <View style={ styles.container }>
                <TouchableWithoutFeedback style={ styles.input } onPress={Keyboard.dismiss} accessible={false}>
                <TextInput style={ styles.colorFont } multiline={ true } onChange={ (e) => {
                    this.handleChange(e, 'title')
                } } placeholder={ 'titre*' } value={ title }/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback style={ styles.input } onPress={Keyboard.dismiss} accessible={false}>
                <TextInput style={ styles.colorFont } multiline={ true } onChange={ (e) => {
                    this.handleChange(e, 'description')
                } } placeholder={ 'description*' } value={ description }/>
                </TouchableWithoutFeedback>
                <FileInput onChange={this.onPhotoSelected}/>
                <Button label="Creer" onPress={ this.submit } className={ [ButtonStyles.btn, ButtonStyles.btnPrimary] }/>
            </View>
        );
    }
}

export interface ISmartEpisodeFormPorps {
    idAnime : number;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartEpisodeFormPorps) => ({
    anime : getEntity(state.animes)(ownProps.idAnime),
    nextEpisodeNumber : getAnimeLastEpisodeNumber(state)(ownProps.idAnime) + 1,
});

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setEpisodes : (episodes : INormalizedData<IEpisode>) => dispatch(setEpisodes(episodes)),
});

export const SmartEpisodeForm = connect(mapStateToProps, mapDispatchToProps)(EpisodeForm);

const styles = StyleSheet.create({
    container : {
        alignItems : 'center',
        justifyContent : 'center',
        marginTop : 24,
        flex : 1,
        marginBottom : 20,
        padding : 5,
        height : 500,
    },
    specificInfos : {
        display : 'flex',
        flexDirection : 'row',
        flexWrap : 'wrap',
    },
    label : {
        marginBottom : 5,
        color : '#fff',
        fontSize : 12,
    },
    input : {
        fontSize : 18,
        borderWidth : 1,
        height : 40,
        padding : 4,
        color : '#fff',
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
        width : 250,
    },
    colorFont : {
        color : "#fff"
    }
});
