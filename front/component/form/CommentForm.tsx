import * as React from 'react';
import { StyleSheet, TextInput, View, Keyboard } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IComment } from '../../../back/src/interface/IComment';
import { IEpisode } from '../../../back/src/interface/IEpisode';
import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { IUser } from '../../../back/src/interface/IUser';
import { Identified } from '../../../back/src/type/Identified';
import { setComments } from '../../action-creator/setComments';
import { normalize } from '../../normalize';
import CommentServiceServiceInstance from '../../service/CommentService';
import { all, getEntity } from '../../utils';
import { Button, ButtonStyles } from '../Button';
import { formStyles } from './LoginForm';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

interface ICommentFormProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    setComments : (comments : INormalizedData<IComment>) => any;
    episode : Identified<IEpisode>;
    userLogged : Identified<IUser>;
}

interface ICommentFormState {
    comment : string;

}

export default class CommentForm extends React.PureComponent<ICommentFormProps, ICommentFormState> {
    public state = {
        comment : '',
    };

    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };
    private setReduxState = async () => {
        const getComments = await CommentServiceServiceInstance.findAll();
        const dataComments = await getComments.json();
        this.props.setComments(normalize(dataComments.comments));
    };
    submit = async (e : any) => {
        const comment = { comment : this.state.comment, idEpisode : this.props.episode.id, idUser : this.props.userLogged.id }
        const response = await CommentServiceServiceInstance.create(comment);
        const data = await response.json();
        console.log(this.state.comment);
        if (data.comment) {
            await this.setReduxState();
            this.setState({
                comment : '',
            })
        }

    };

    render() {
        return (
            <View style={ styles.container }>
                <TouchableWithoutFeedback style={ styles.commentInput } onPress={Keyboard.dismiss} accessible={false}>
                <TextInput style={ styles.colorFont } multiline={ true } onChange={ (e) => {
                    this.handleChange(e, 'comment')
                } } placeholder={ 'commentaire' } value={ this.state.comment }/>
                </TouchableWithoutFeedback>
                <Button label="Envoyer" onPress={ this.submit } className={ [ButtonStyles.btn, ButtonStyles.btnPrimary] }/>
            </View>
        );
    }
}

export interface ISmartCommentFormProps {
    idEpisode : number
}

interface IStoreState {
    episodes : INormalizedData<IEpisode>;
    userLogged : INormalizedData<IUser>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartCommentFormProps) => ({
    episode : getEntity(state.episodes)(ownProps.idEpisode),
    userLogged : all(state.userLogged)[0],
});

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setComments : (comments : INormalizedData<IComment>) => dispatch(setComments(comments)),
});

export const SmartCommentForm = connect(mapStateToProps, mapDispatchToProps)(CommentForm);

const styles = StyleSheet.create({
    container : {
        alignItems : 'center',
        justifyContent : 'center',
        marginTop : 24,
        flex : 1,
        marginBottom : 20,
        padding : 5,
        height : 500,
    },
    commentInput : {
        height : 150,
        borderWidth : 1,
        padding : 4,
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
        width : 250,
    },
    colorFont : {
        color : "#fff"
    }
});
