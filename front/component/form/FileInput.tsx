import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Button, ButtonStyles } from '../Button';

interface IFileInputProps {
    onChange : (uri? : string | ArrayBuffer | null) => any;
}

interface IFileInputState {
    uri? : string | ArrayBuffer | null;
}

export default class FileInput extends React.PureComponent<IFileInputProps, IFileInputState> {
    public state = {
        uri : undefined,
    };

    componentDidMount() {
        this.getPermissionAsync();
    }

    getPermissionAsync = async () => {
        if (Constants.platform!.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };

    onPhotoSelected = async (uri : any) => {
        const response = await fetch(uri);
        const blob = await response.blob();
        const a = new FileReader();
        a.onload = (e) => {
            this.setState({ uri : e.target?.result });
            this.props.onChange(e.target?.result);
        };
        a.readAsDataURL(blob);
    };

    pickFile = async () => {
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes : ImagePicker.MediaTypeOptions.Images,
                allowsEditing : true,
                aspect : [4, 3],
                quality : 1,
            });
            if (!result.cancelled) {
                this.onPhotoSelected(result.uri)
            }

        } catch (E) {
            console.log(E);
        }

    };

    render() {
        const { uri } = this.state;
        return (

            <View>
                { uri && <Image source={ { uri : uri } } style={ { height : 80 } }/> }
                <Button icon="file" color="#fff" size={ 18 } label="choisir une image*" onPress={ this.pickFile }
                        className={ [ButtonStyles.btn, ButtonStyles.btn] }/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container : {
        alignItems : 'center',
        justifyContent : 'center',
    },

    input : {
        fontSize : 18,
        borderWidth : 1,
        height : 40,
        padding : 4,
        color : '#fff',
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
        width : 50,
    },
});
