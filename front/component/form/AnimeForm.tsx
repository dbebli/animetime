import * as React from 'react';
import { Keyboard, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../../back/src/interface/IAnime';
import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { setAnimes } from '../../action-creator/setAnimes';
import { normalize } from '../../normalize';
import AnimeServiceServiceInstance from '../../service/AnimeService';
import { Button, ButtonStyles } from '../Button';
import FileInput from './FileInput';
import { formStyles } from './LoginForm';

interface IAnimeFormFormProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    setAnimes : (comments : INormalizedData<IAnime>) => any;
}

interface IAnimeFormState {
    yearReleased : string;
    releaseDay : string;
    releaseHour : string;
    episodeDuration : number;
    cover? : string | ArrayBuffer | null;
    title : string;
    description : string;
}

export default class AnimeForm extends React.PureComponent<IAnimeFormFormProps, IAnimeFormState> {
    public state = {
        yearReleased : '',
        releaseDay : '',
        releaseHour : '',
        episodeDuration : 0,
        cover : undefined,
        title : '',
        description : '',
    };

    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };
    onPhotoSelected = (uri? : string | ArrayBuffer | null) => {
        this.setState({ cover : uri })
    };

    private setReduxState = async () => {
        const getAnimes = await AnimeServiceServiceInstance.findAll();
        const dataAnimes = await getAnimes.json();
        this.props.setAnimes(normalize(dataAnimes.animes));
    };
    submit = async (e : any) => {
        const response = await AnimeServiceServiceInstance.create(this.state);
        const data = await response.json();
        if (data.anime) {
            await this.setReduxState();
            this.setState({
                yearReleased : '',
                releaseDay : '',
                releaseHour : '',
                episodeDuration : 0,
                cover : undefined,
                title : '',
                description : '',
            })
        }

    };

    render() {
        const { description, releaseDay, releaseHour, yearReleased, cover, episodeDuration, title } = this.state;
        return (
            <View style={ styles.container }>
                <TouchableWithoutFeedback onPress={ Keyboard.dismiss } accessible={ false }>
                    <TextInput multiline={ true } style={ [formStyles.input] } onChange={ (e) => {
                        this.handleChange(e, 'title')
                    } } placeholder={ 'titre*' } value={ title }/>
                </TouchableWithoutFeedback>
                <TextInput multiline={ true } style={ [formStyles.input] } onChange={ (e) => {
                    this.handleChange(e, 'yearReleased')
                } } keyboardType={ 'numeric' } placeholder={ 'année de sortie*' } value={ yearReleased }/>
                <TextInput multiline={ true } style={ [formStyles.input] } onChange={ (e) => {
                    this.handleChange(e, 'description')
                } } placeholder={ 'description*' } value={ description }/>
                <FileInput onChange={ this.onPhotoSelected }/>

                <View style={ styles.specificInfos }>
                    <View>
                        <Text style={ styles.label }>jour de sortie*</Text>
                        <TextInput multiline={ true } style={ [styles.input] } onChange={ (e) => {
                            this.handleChange(e, 'releaseDay')
                        } } value={ releaseDay }/>
                    </View>
                    <View>
                        <Text style={ styles.label }>heure de sortie*</Text>
                        <TextInput multiline={ true } style={ [styles.input] } onChange={ (e) => {
                            this.handleChange(e, 'releaseHour')
                        } } keyboardType={ 'numeric' } value={ releaseHour }/>
                    </View>
                    <View>
                        <Text style={ styles.label }>Durée d'un episode*</Text>
                        <TextInput multiline={ true } style={ [styles.input] } onChange={ (e) => {
                            this.handleChange(e, 'episodeDuration')
                        } } keyboardType={ 'numeric' } placeholder={ 'episodeDuration' } value={ String(episodeDuration) }/>
                    </View>

                </View>

                <Button label="Creer" onPress={ this.submit } className={ [ButtonStyles.btn, ButtonStyles.btnPrimary] }/>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setAnimes : (animes : INormalizedData<IAnime>) => dispatch(setAnimes(animes)),
});

export const SmartAnimeForm = connect(undefined, mapDispatchToProps)(AnimeForm);

const styles = StyleSheet.create({
    container : {
        alignItems : 'center',
        justifyContent : 'center',
        marginTop : 24,
        flex : 1,
        marginBottom : 20,
        padding : 5,
        height : 500,
    },
    specificInfos : {
        display : 'flex',
        flexDirection : 'row',
    },
    label : {
        marginBottom : 5,
        color : '#fff',
        fontSize : 12,
    },
    input : {
        fontSize : 18,
        borderWidth : 1,
        height : 40,
        padding : 4,
        color : '#fff',
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
        width : 50,
    },
});
