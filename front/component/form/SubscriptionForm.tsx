import * as React from 'react';
import { Image, StyleSheet, TextInput, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { INormalizedData } from '../../../back/src/interface/INormalizedData';
import { IUser } from '../../../back/src/interface/IUser';
import { setUsers } from '../../action-creator/user/setUsers';
import UserServiceInstance from '../../service/UserService';
import { Button, ButtonStyles } from '../Button';
import FileInput from './FileInput';
import { formStyles } from './LoginForm';

interface ISubscriptionFormProps {
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
    setUsers : (users : INormalizedData<IUser>) => any;
}

interface ISubscriptionFormState {
    username : string;
    password : string;
    email : string;
    avatar? : string | ArrayBuffer | null;
}

export default class SubscriptionForm extends React.PureComponent<ISubscriptionFormProps, ISubscriptionFormState> {
    public state = {
        username : '',
        password : '',
        email : '',
        avatar : undefined,
    };

    componentDidMount() {

    }

    handleChange = (e : any, name : string) => {
        // @ts-ignore
        this.setState({
            [name] : e.nativeEvent.text
        })
    };

    onPhotoSelected = (uri? : string | ArrayBuffer | null) => {
        this.setState({ avatar : uri })
    };

    submit = async (e : any) => {
        const response = await UserServiceInstance.create(this.state);
        const data = await response.json();
        if (data.user) {
            this.setState({
                username : '',
                password : '',
                email : '',
                avatar : null,
            })
        }

    };

    render() {
        const { password, username, avatar, email } = this.state;
        return (
            <View style={ styles.container }>
                <TextInput style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'username')
                } } placeholder={ 'nom d\'utilisateur' } value={ username }/>

                <TextInput style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'email')
                } } placeholder={ 'email' } value={ email }/>
                <TextInput style={ formStyles.input } onChange={ (e) => {
                    this.handleChange(e, 'password')
                } } placeholder={ 'mdp' } secureTextEntry={ true } value={ password }/>
                <View style={ avatar ? { height : 130 } : { height : 50 } }>
                    <FileInput onChange={ this.onPhotoSelected }/>
                </View>

                <Button label="S'inscrire" onPress={ this.submit } className={ [ButtonStyles.btn, ButtonStyles.btnPrimary] }/>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setUsers : (users : INormalizedData<IUser>) => dispatch(setUsers(users)),

});

export const SmartSubscriptionForm = connect(undefined, mapDispatchToProps)(SubscriptionForm);

const styles = StyleSheet.create({
    container : {
        marginTop : 24,
        flex : 1,
        marginBottom : 20,
        padding : 5,
        height : 'auto',
    },
    input : {
        fontSize : 18,
        borderWidth : 1,
        height : 40,
        padding : 4,
        color : '#fff',
        marginBottom : 15,
        marginHorizontal : 20,
        borderColor : 'grey',
    },
});
