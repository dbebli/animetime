import { isEqual } from 'lodash';
import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { IComment } from '../../back/src/interface/IComment';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { Identified } from '../../back/src/type/Identified';
import { setComments } from '../action-creator/setComments';
import avatar from '../assets/defaultavatar.png';
import { normalize } from '../normalize';
import CommentServiceServiceInstance from '../service/CommentService';
import { all, getEntity } from '../utils';

interface ICommentProps {
    user : Identified<IUser>;
    userLogged : Identified<IUser>;
    comment : Identified<IComment>;
    setComments : (comments : INormalizedData<IComment>) => any;
}

export default class Comment extends React.PureComponent<ICommentProps> {
    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { comment, user, userLogged } = this.props;
        const deleteButton = isEqual(user, userLogged) ? <TouchableOpacity style={ styles.deleteButton } onPress={ this.deleteComment }>
            <Icon name='trash' color="#fff" size={ 24 }/>
        </TouchableOpacity> : null;
        return (
            <View style={ styles.container }>
                <View style={ styles.commentHeader }>
                    { deleteButton }
                    <Image style={ styles.image } source={ avatar }/>
                    <View>
                        <Text style={ styles.username }>{ user.username }</Text>
                        <Text style={ styles.date }>{ comment.createdAt }</Text>
                    </View>
                </View>
                <Text style={ styles.text }>{ comment.comment }</Text>
            </View>
        )
    }

    private setReduxState = async () => {
        const getComments = await CommentServiceServiceInstance.findAll();
        const dataComments = await getComments.json();
        this.props.setComments(normalize(dataComments.comments));
    };

    private deleteComment = async () => {
        await CommentServiceServiceInstance.remove(this.props.comment.id);
        await this.setReduxState();
    }
}

export interface ISmartCommentProps {
    idComment : number;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    comments : INormalizedData<IComment>;
    users : INormalizedData<IUser>;
    userLogged : INormalizedData<IUser>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartCommentProps) => {
    const comment = getEntity(state.comments)(ownProps.idComment);
    return {
        comment,
        user : getEntity(state.users)(comment.idUser),
        userLogged : all(state.userLogged)[0],
    }
};
const mapDispatchToProps = (dispatch : Dispatch) => ({
    setComments : (comments : INormalizedData<IComment>) => dispatch(setComments(comments)),
});

export const SmartComment = connect(mapStateToProps, mapDispatchToProps)(Comment);

const styles = StyleSheet.create({
    container : {
        borderRadius : 5,
        display : 'flex',
        width : '90%',
        justifyContent : 'center',
        marginVertical : 10,
        marginHorizontal : 10,
        paddingVertical : 10,
        paddingHorizontal : 15,
        backgroundColor : '#4a4848',
    },
    text : {
        fontSize : 16,
        color : '#fff',
    },
    username : {
        fontSize : 14,
        color : '#fff',
    },
    date : {
        fontSize : 15,
        color : '#a6a6a6',
    },
    commentHeader : {
        display : 'flex',
        alignItems : 'center',
        flexDirection : 'row',
        marginBottom : 10,
    },
    deleteButton : {
        position : 'absolute',
        right : 0,
        top : 5,
    },
    image : {
        borderRadius : 50,
        height : 30,
        marginRight : 15,
        width : 30,
    },
});
