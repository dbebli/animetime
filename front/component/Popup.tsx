import { ReactNode } from 'react';
import * as React from 'react';
import { Modal, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export interface IPopupProviderProps {
    isVisible : boolean;
    title : string,
    hideModal : () => any;
    content : ReactNode,
}

export interface IPopupProviderState {
    isVisible : boolean,
    title : string,
    content : ReactNode,
}

export class Popup extends React.PureComponent<IPopupProviderProps, IPopupProviderState> {
    // @ts-ignore

    public state : IPopupProviderState = {
        isVisible : this.props.isVisible,
        title : this.props.title,
        content : this.props.content,
    };

    public componentDidUpdate(prevProps : Readonly<IPopupProviderProps>, prevState : Readonly<IPopupProviderState>, snapshot? : any) : void {
        this.setState({
            isVisible : prevProps.isVisible !== this.props.isVisible ? this.props.isVisible : this.state.isVisible,
            title : prevProps.title !== this.props.title ? this.props.title : this.state.title,
            content : prevProps.content !== this.props.content ? this.props.content : this.state.content,
        });
    }

    toggleShow = (title : string, content : ReactNode) => {
        this.setState({
            isVisible : !this.state.isVisible,
            title,
            content,
        });
    };

    render() {
        const { hideModal } = this.props;
        const { isVisible, title, content } = this.state;
        const displayTitle = typeof title !== 'string' ? null : <Text style={ styles.headerTitle }>{ title }</Text>;
        return (
            <Modal visible={ isVisible } animated={ true } animationType='fade' transparent={ true }>
                <View style={ styles.centeredView }>
                    {/*<View style={ styles.inner }>*/ }
                    {/*    <View style={ styles.header }>*/ }
                    {/*        { displayTitle }*/ }
                    {/*    </View>*/ }
                    {/*    <View style={ styles.content }>*/ }
                    {/*        { content }*/ }
                    {/*    </View>*/ }
                    {/*</View>*/ }
                    <View style={ styles.modalView }>
                        <View style={ styles.content }>{ content }</View>
                        <TouchableHighlight
                            style={ styles.openButton }
                            onPress={ hideModal
                            }
                        >
                            <Icon name='times' color="#1accf0" size={ 24 }/>
                        </TouchableHighlight>
                    </View>

                </View>

            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    centeredView : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        marginTop : 22
    },
    content : {
        width : '95%',
        height : 380,
    },

    headerTitle : {
        color : '#fff',
        lineHeight : 10,
        fontWeight : 'bold',
    },

    header : {
        height : 30,
        backgroundColor : '#1accf0',
        width : '100%',
        fontSize : 16,
        display : 'flex',
        justifyContent : 'space-between',
        padding : 15,
    },

    modalView : {
        margin : 20,
        borderWidth : 4,
        borderColor : '#1accf0',
        width : '95%',
        backgroundColor : '#242323',
        borderRadius : 10,
        padding : 35,
        alignItems : 'center',
        shadowColor : '#000',
        shadowOffset : {
            width : 0,
            height : 2
        },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation : 5
    },
    openButton : {
        borderRadius : 20,
        position : 'absolute',
        right : 5,
        elevation : 2
    },
    textStyle : {
        color : 'white',
        fontWeight : 'bold',
        textAlign : 'center'
    },

});
