import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import { setEpisodes } from '../action-creator/setEpisodes';
import { setSelectedEpisode } from '../action-creator/setSelectedEpisode';
import { setViews } from '../action-creator/setViews';
import defaultCover from '../assets/defaultCover.png';
import { normalize } from '../normalize';
import EpisodeService from '../service/EpisodeService';
import ViewService from '../service/ViewService';
import { all, filter, getEntity } from '../utils';

export interface IEpisodeProps {
    episode : Identified<IEpisode>;
    setViews : (views : INormalizedData<IView>) => any;
    userLogged : Identified<IUser>;
    anime : Identified<IAnime>;
    deletetable? : boolean;
    view : Identified<IView> | undefined;
    setEpisodes : (episodes : INormalizedData<IEpisode>) => any;
    setSelectedEpisode : (episode : INormalizedData<IEpisode>) => any;
    navigation : NavigationScreenProp<NavigationState, NavigationParams>;
}

export default class Episode extends React.PureComponent<IEpisodeProps> {

    componentDidMount() {
        this.props.setSelectedEpisode(normalize([this.props.episode]));
    }

    componentDidUpdate() {
        this.props.setSelectedEpisode(normalize([this.props.episode]));
    }

    render() {
        const { episode, view, anime, deletetable } = this.props;
        const buttonSize = deletetable ? 28 : 40;
        const viewButton = view && view.state === 'S' ?
            <Icon name="check-circle" color='#10cc13' size={ buttonSize }/> : <Icon name="check-circle" color='#fff' size={ buttonSize }/>;
        const deleteButton = deletetable ?
            <TouchableOpacity onPress={ this.deleteEpisode } style={ styles.button }>
                <Icon name="trash" color='#fff' size={ buttonSize }/>
            </TouchableOpacity> : null;
        return (
            <View style={ styles.container }>
                { anime ?
                    <>
                        { episode.cover && episode.cover !== '' ? <Image source={ { uri : episode.cover } } style={ styles.headerImage } resizeMode="cover"/> :
                            <Image source={ defaultCover } style={ styles.headerImage }
                                   resizeMode={ 'cover' }/>
                        }
                        <View style={ styles.subContainer }>
                            <TouchableOpacity onPress={ this.seeComments } style={ {
                                width : '80%', height : '100%',
                            } }>
                                <View style={ styles.body }>
                                    <TouchableOpacity onPress={ this.seeAnimeDetails }>
                                        <Text style={ styles.animeTitle }> { anime.title }</Text>
                                    </TouchableOpacity>
                                    <Text style={ styles.title }>Episode { episode.episodeNumber }</Text>
                                    <Text style={ styles.subtitle }>{ episode.title }</Text>
                                </View>
                            </TouchableOpacity>
                            <View>
                                <TouchableOpacity onPress={ this.toggleView } style={ styles.button }>
                                    { viewButton }
                                </TouchableOpacity>
                                { deleteButton }
                            </View>
                        </View>
                    </>
                    : null
                }


            </View>
        );
    }

    private seeComments = () => {
        this.props.setSelectedEpisode(normalize([this.props.episode]));
        this.props.navigation.navigate('EpisodeComments');
    };

    private seeAnimeDetails = () => {
        this.props.navigation.navigate(
            'AnimeDetails',
            { idAnime : this.props.anime.id }
        );

    };

    private deleteEpisode = async () => {
        await EpisodeService.remove(this.props.episode.id);
        await this.setReduxState();
    };

    private toggleView = async () => {
        await ViewService.create({ idEpisode : this.props.episode.id, idUser : this.props.userLogged.id });
        await this.setReduxState();
    };

    private setReduxState = async () => {
        const getViews = await ViewService.findAll();
        const dataViews = await getViews.json();
        this.props.setViews(normalize(dataViews.views));
        const getEpisodes = await EpisodeService.findAll();
        const dataEpisodes = await getEpisodes.json();
        this.props.setEpisodes(normalize(dataEpisodes.episodes));
    }
}

export interface ISmartAnimeProps {
    idEpisode : number;
}

interface IStoreState {
    animes : INormalizedData<IAnime>;
    userLogged : INormalizedData<IUser>;
    episodes : INormalizedData<IEpisode>;
    views : INormalizedData<IView>;
}

const mapStateToProps = (state : IStoreState, ownProps : ISmartAnimeProps) => {
    const episode = getEntity(state.episodes)(ownProps.idEpisode);
    const userLogged = all(state.userLogged)[0];
    return {
        episode,
        userLogged,
        anime : filter(state.animes)(anime => anime.id === episode.idAnime)[0],
        view : filter(state.views)(view => view.idEpisode === episode.id && view.idUser === userLogged.id)[0],
    }
};

const mapDispatchToProps = (dispatch : Dispatch) => ({
    setViews : (views : INormalizedData<IView>) => dispatch(setViews(views)),
    setEpisodes : (episodes : INormalizedData<IEpisode>) => dispatch(setEpisodes(episodes)),
    setSelectedEpisode : (episode : INormalizedData<IEpisode>) => dispatch(setSelectedEpisode(episode)),
});

export const SmartEpisode = connect(mapStateToProps, mapDispatchToProps)(Episode);

const styles = StyleSheet.create({
    body : {
        display : 'flex',
        justifyContent : 'center',
        padding : 5,
        borderBottomEndRadius : 20,
        borderBottomStartRadius : 20,
    },
    button : {
        margin : 4
    },
    container : {
        display : 'flex',
        flexDirection : 'row',
        borderRadius : 5,
        alignItems : 'center',
        justifyContent : 'flex-start',
        marginVertical : 10,
        marginHorizontal : 10,
        backgroundColor : '#4a4848',
        height : 80,
    },
    subContainer : {
        position : 'relative',
        display : 'flex',
        paddingRight : 6,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'space-between',
        width : '80%',
        height : '100%',
    },
    title : {
        fontSize : 17,
        fontWeight : 'bold',
        color : '#fff'
    },

    animeTitle : {
        fontSize : 14,
        borderRadius : 15,
        width : 150,
        fontWeight : 'bold',
        color : '#fff',
        padding : 1,
        borderWidth : 2,
        borderColor : '#fff',
    },

    subtitle : {
        fontSize : 16,
        color : '#fff'
    },
    headerImage : {
        height : '100%',
        borderTopLeftRadius : 5,
        borderBottomLeftRadius : 5,
        width : '20%',
    }
});

