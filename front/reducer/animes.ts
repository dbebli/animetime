import { SetAnimes } from '../action-creator/setAnimes';

const initialState : [] = [];

const animes = (state = initialState, action : any) => {
    switch (action.type) {
        case SetAnimes:
            return action.payload.animes;

        default:
            return state;
    }
};

export default animes;