import { SetSelectedEpisode } from '../action-creator/setSelectedEpisode';

const initialState = null;

const selectedEpisode = (state = initialState, action : any) => {
    switch (action.type) {
        case SetSelectedEpisode:
            return action.payload.episode;

        default:
            return state;
    }
};

export default selectedEpisode;