import { SetUserAnimes } from '../action-creator/setUserAnimes';

const initialState : [] = [];

const userAnimes = (state = initialState, action : any) => {
    switch (action.type) {
        case SetUserAnimes:
            return action.payload.userAnimes;

        default:
            return state;
    }
};

export default userAnimes;