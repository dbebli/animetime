import { SetEpisodes } from '../action-creator/setEpisodes';

const initialState : [] = [];

const episodes = (state = initialState, action : any) => {
    switch (action.type) {
        case SetEpisodes:
            return action.payload.episodes;

        default:
            return state;
    }
};

export default episodes;