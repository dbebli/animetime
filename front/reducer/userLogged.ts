import { SetUserLogged } from '../action-creator/user/setUserLogged';

const initialState = null;

const userLogged = (state = initialState, action : any) => {
    switch (action.type) {
        case SetUserLogged:
            return action.payload.userLogged;

        default:
            return state;
    }
};

export default userLogged;