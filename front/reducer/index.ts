import { combineReducers } from 'redux';
import animes from './animes';
import comments from './comments';
import episodes from './episodes';
import selectedEpisode from './selectedEpisode';
import setSelectedEpisode from './selectedEpisode';
import userAnimes from './userAnimes';
import userLogged from './userLogged';
import users from './users';
import views from './views';

export default combineReducers({
    users,
    userLogged,
    animes,
    comments,
    episodes,
    userAnimes,
    views,
    selectedEpisode,
});