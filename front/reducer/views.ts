import { SetViews } from '../action-creator/setViews';

const initialState : [] = [];

const views = (state = initialState, action : any) => {
    switch (action.type) {
        case SetViews:
            return action.payload.views;

        default:
            return state;
    }
};

export default views;