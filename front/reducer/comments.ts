import { SetComments } from '../action-creator/setComments';

const initialState : [] = [];

const comments = (state = initialState, action : any) => {
    switch (action.type) {
        case SetComments:
            return action.payload.comments;

        default:
            return state;
    }
};

export default comments;