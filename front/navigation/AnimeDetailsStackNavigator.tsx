import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Route, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { TransitionPresets } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { Identified } from '../../back/src/type/Identified';
import { SmartAnimeEpisodes } from '../component/AnimeEpisodes';
import AnimeDetails from '../screen/AnimeDetails';
import { SmartEpisodeComments } from '../screen/EpisodeComments';
import { getEntity } from '../utils';

const Stack = createStackNavigator();

export interface IAnimeDetailsStackNavigatorProps {
    route : Route;
}

export default class AnimeDetailsStackNavigator extends React.PureComponent<IAnimeDetailsStackNavigatorProps> {
    public render() : React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const { route } = this.props;
        return (
            <Stack.Navigator initialRouteName='Episode' screenOptions={ ({ route, navigation }) => ({
                gestureEnabled : true,
                cardOverlayEnabled : true,
                headerShown : false,
                headerStatusBarHeight :
                    navigation.dangerouslyGetState().routes.indexOf(route) > 0
                        ? 0
                        : undefined,
                ...TransitionPresets.ModalPresentationIOS,
            }) }
                             mode="modal"

            >
                <Stack.Screen initialParams={ { idAnime : route.params.idAnime } } options={ { cardStyle : { backgroundColor : '#242323' } } } name="Episode"
                              component={ SmartAnimeEpisodes }/>
                <Stack.Screen initialParams={ { idAnime : route.params.idAnime } } options={ { cardStyle : { backgroundColor : '#242323' } } }
                              name="EpisodeComments"
                              component={ SmartEpisodeComments }/>
            </Stack.Navigator>
        );

    }
}

const styles = StyleSheet.create({
    title : {
        fontSize : 18,
        color : '#fff',
        marginLeft : 5,
    },
    stats : {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        marginHorizontal : 5,
        marginVertical : 5,
    },

});