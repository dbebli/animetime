import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { Identifier } from '../../back/src/type/Identifier';
import { filter, getEntity } from '../utils';

interface IStoreState {
    episodes : INormalizedData<IEpisode>;
    animes : INormalizedData<IAnime>;
    userAnimes : INormalizedData<IUserAnime>;
}

export const getAnimeFollowerNumber = (state : IStoreState) => (idAnime : Identifier) : number => {
    const anime = getEntity(state.animes)(idAnime);
    const userAnimeFollowers = filter(state.userAnimes)(userAnime => userAnime.idAnime === anime.id)
    return userAnimeFollowers.length
};