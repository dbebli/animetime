import { IAnime } from '../../back/src/interface/IAnime';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { Identified } from '../../back/src/type/Identified';
import { filter } from '../utils';

interface IStoreState {
    animes : INormalizedData<IAnime>;
    userAnimes : INormalizedData<IUserAnime>;
}

export const getUserAnimeList = (state : IStoreState) => (idUser : number) : Identified<IAnime>[] => {
    const filteredUserAnime = filter(state.userAnimes)(userAnime => userAnime.idUser === idUser);
    const animeIds = filteredUserAnime.map(userAnime => userAnime.idAnime);
    return filter(state.animes)(anime => animeIds.includes(anime.id))
};