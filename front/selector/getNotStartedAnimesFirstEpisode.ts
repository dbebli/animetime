import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import { all, filter } from '../utils';
import { getNotViewAnimesFirstEpisode } from './getNotViewAnimesFirstEpisode';

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
    views : INormalizedData<IView>;
    userLogged : INormalizedData<IUser>;
}

export const getNotStartedAnimesFirstEpisode = (state : IStoreState) => (animes : Identified<IAnime>[]) : Identified<IEpisode>[] => {
    const userLogged = all(state.userLogged)[0];
    const views = filter(state.views)(view => view.idUser === userLogged.id);
    const animesFirstEpisodes = getNotViewAnimesFirstEpisode(state)(animes);
    const viewEpisodeIds = views.map(view => view.idEpisode);
    const unseen = views.filter(view => view.state === 'UN');
    const unseenEpisodeIds = unseen.map(unseen => unseen.idEpisode);
    return animesFirstEpisodes.filter(episode => !viewEpisodeIds.includes(episode.id) || unseenEpisodeIds.includes(episode.id));
};