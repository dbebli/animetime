import { groupBy, map } from 'lodash';
import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import { all, filter, getEntity } from '../utils';
import { animeHasFirstEpisodeViewed } from './animeHasFirstEpisodeViewed';

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
    views : INormalizedData<IView>;
    userLogged : INormalizedData<IUser>;
}

export const getAnimesEpisodeToWatch = (state : IStoreState) => (animes : Identified<IAnime>[]) : Identified<IEpisode>[] => {
    const animeIds = animes.map(anime => anime.id);
    const userLogged = all(state.userLogged)[0];
    const seen = filter(state.views)(view => view.state === 'S' && view.idUser === userLogged.id);
    const seenEpisodeIds = seen.map(seen => seen.idEpisode);
    const episodesSeen = filter(state.episodes)(episode => seenEpisodeIds.includes(episode.id));
    const episodesGroupedByAnime = groupBy(episodesSeen, episode => {
        const anime = getEntity(state.animes)(episode.idAnime);
        return animeIds.includes(anime.id) && anime.id;
    });
    const animesHasFirstEpisodeViewed = filter(state.animes)(anime => animeHasFirstEpisodeViewed(state)(anime));
    const animesHasFirstEpisodeViewedIds = animesHasFirstEpisodeViewed.map(anime => anime.id);

    const lastEpisodesSeen = map(episodesGroupedByAnime, (episodes) => {
        const sortedEpisodes = episodes.sort((a, b) => a.episodeNumber - b.episodeNumber);
        return sortedEpisodes[sortedEpisodes.length - 1];
    });
    const toSee = new Set<Identified<IEpisode>>();
    lastEpisodesSeen.forEach(episode => {
        const nextEpisode = filter(state.episodes)(
            nextEpisode => animeIds.includes(nextEpisode.idAnime) && animesHasFirstEpisodeViewedIds.includes(nextEpisode.idAnime) && nextEpisode.episodeNumber ===
                           (episode.episodeNumber + 1))[0];
        nextEpisode && toSee.add(nextEpisode)
    });
    return Array.from(toSee).filter(episode => !seenEpisodeIds.includes(episode.id))
};