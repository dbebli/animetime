import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import { all, filter } from '../utils';

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
    views : INormalizedData<IView>;
    userLogged : INormalizedData<IUser>;
}

export const animeHasFirstEpisodeViewed = (state : IStoreState) => (anime : Identified<IAnime>) : boolean => {
    const userLogged = all(state.userLogged)[0];
    const userViews = filter(state.views)(view => view.idUser === userLogged.id && view.state === 'S');
    const viewEpisodeIds = userViews.map(userView => userView.idEpisode);
    const animeEpisodes = filter(state.episodes)(episode => episode.episodeNumber === 1 && episode.idAnime === anime.id && viewEpisodeIds.includes(episode.id));
    console.log(anime.title, !!animeEpisodes.length)
    return !!animeEpisodes.length
};