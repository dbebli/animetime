import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { Identifier } from '../../back/src/type/Identifier';
import { filter, getEntity } from '../utils';

interface IStoreState {
    episodes : INormalizedData<IEpisode>;
    animes : INormalizedData<IAnime>;
}

export const getAnimeLastEpisodeNumber = (state : IStoreState) => (idAnime : Identifier) : number => {
    const anime = getEntity(state.animes)(idAnime);
    const episodes = filter(state.episodes)(episode => episode.idAnime === anime.id);
    const sortedEpisodes = episodes.sort((a, b) => a.episodeNumber - b.episodeNumber);
    return episodes.length ? sortedEpisodes[sortedEpisodes.length - 1].episodeNumber : 0;
};