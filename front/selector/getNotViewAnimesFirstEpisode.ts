import { IAnime } from '../../back/src/interface/IAnime';
import { IEpisode } from '../../back/src/interface/IEpisode';
import { INormalizedData } from '../../back/src/interface/INormalizedData';
import { IUser } from '../../back/src/interface/IUser';
import { IUserAnime } from '../../back/src/interface/IUserAnime';
import { IView } from '../../back/src/interface/IView';
import { Identified } from '../../back/src/type/Identified';
import { all, filter } from '../utils';
import { animeHasView } from './animeHasView';

interface IStoreState {
    animes : INormalizedData<IAnime>;
    episodes : INormalizedData<IEpisode>;
    views : INormalizedData<IView>;
    userLogged : INormalizedData<IUser>;
}

export const getNotViewAnimesFirstEpisode = (state : IStoreState) => (animes : Identified<IAnime>[]) : Identified<IEpisode>[] => {
    const episodes = new Set<Identified<IEpisode>>();
    animes.forEach(anime => {
        const isViewed = animeHasView(state)(anime);
        const episode = filter(state.episodes)(episode => episode.idAnime === anime.id && episode.episodeNumber === 1)[0]
        !isViewed && episode && episodes.add(episode);
    });
    return episodes.size ? Array.from(episodes) : []
};