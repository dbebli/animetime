import { getRepository } from 'typeorm';
import { View } from '../entity/View';

export const getView = async (idUser : number, idEpisode : number) => await getRepository(View)
    .createQueryBuilder()
    .select('*')
    .where('idUser = :idUser and idEpisode = :idEpisode', { idUser : idUser, idEpisode : idEpisode })
    .getRawOne();

export const insertView = async (view : View) => getRepository(View).save(view);
