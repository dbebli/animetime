import { getRepository } from 'typeorm';
import { Episode } from '../entity/Episode';


export const getAllCommentByEpisode = async (id : number) => await getRepository(Episode)
    .createQueryBuilder()
    .select('*')
    .where('idEpisode = :id', { id : id })
    .getRawMany();

export const insertEpisode = async (episode : Episode) => getRepository(Episode).save(episode);
