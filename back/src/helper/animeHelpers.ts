import { getRepository } from 'typeorm';
import { Anime } from '../entity/Anime';


export const getAllAnimes = async () => await getRepository(Anime)
    .createQueryBuilder()
    .select('*')
    .getRawMany();

export const getAnimeById = async (id : number) => await getRepository(Anime)
    .createQueryBuilder()
    .select('*')
    .where('id = :id', { id : id })
    .getRawOne();

    export const insertAnime = async (anime : Anime) => getRepository(Anime).save(anime);
