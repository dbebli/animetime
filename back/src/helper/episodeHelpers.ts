import { getRepository } from 'typeorm';
import { Episode } from '../entity/Episode';


export const getAllEpisodesByAnime = async (id : number) => await getRepository(Episode)
    .createQueryBuilder()
    .select('*')
    .where('idAnime = :id', { id : id })
    .getRawMany();

export const getEpisodeById = async (id : number) => await getRepository(Episode)
    .createQueryBuilder()
    .select('*')
    .where('id = :id', { id : id })
    .getRawOne();

export const insertEpisode = async (episode : Episode) => getRepository(Episode).save(episode);
