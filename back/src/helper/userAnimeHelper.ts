import { getRepository } from 'typeorm';
import { UserAnime } from '../entity/UserAnime';
import { Identifier } from '../type/Identifier';

export const getUserAnime = async (idUser : Identifier, idAnime : Identifier) => await getRepository(UserAnime)
    .createQueryBuilder()
    .select('*')
    .where('idUser = :idUser and idAnime = :idAnime', { idUser : idUser, idAnime : idAnime })
    .getRawOne();
