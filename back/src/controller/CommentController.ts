import { Request, Response } from 'express';
import moment from 'moment';
import { getRepository } from 'typeorm';
import { Comment } from '../entity/Comment';
import { getEpisodeById } from '../helper/episodeHelpers';
import { getUserById } from '../helper/userHelpers';

export default class CommentController {

    static async getAllComments(req : Request, res : Response) {
        try {
            const comments = await getRepository(Comment)
                .createQueryBuilder()
                .select('*')
                .getRawMany();

            return res.status(200).json({
                comments
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async addComment(req : Request, res : Response) {
        const idUser = req.body.idUser;
        const idEpisode = req.body.idEpisode;
        const userEntity = await getUserById(parseInt(idUser));
        const episodeEntity = await getEpisodeById(parseInt(idEpisode));
        const commentEntity = new Comment();
        try {
            commentEntity.comment = req.body.comment;
            commentEntity.user = userEntity;
            commentEntity.episode = episodeEntity;
            commentEntity.createdAt = moment().locale('fr').format('DD-MM-YYYY');
            commentEntity.idCommentReference = req.body.idCommentReference;
            const comment = await getRepository(Comment).save(commentEntity);

            return res.status(200).json({
                comment,
                message : 'ajout favoris reussi'
            });

        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }

    }

    static async deleteComment(req : Request, res : Response) {
        const idComment = req.params.id;
        try {
            await getRepository(Comment).delete(idComment);
            return res.status(200).json({
                message : 'Suppression réussie'
            });
        } catch (e) {
            return res.status(500).json({
                'message' : e.message
            });
        }

    }

}