import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { UserAnime } from '../entity/UserAnime';
import { View } from '../entity/View';
import { getEpisodeById } from '../helper/episodeHelpers';
import { getUserById } from '../helper/userHelpers';
import { getView } from '../helper/ViewHelpers';
import { ViewState } from '../type/ViewState';

export default class ViewController {

    static async getAll(req : Request, res : Response) {
        try {
            const views = await getRepository(View)
                .createQueryBuilder()
                .select('*')
                .getRawMany();

            return res.status(200).json({
                views
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async toggleView(req : Request, res : Response) {
        const idUser = req.body.idUser;
        const idEpisode = req.body.idEpisode;
        const existingView = await getView(idUser, idEpisode);
        try {
            const viewState = existingView && existingView.state === ViewState.Seen ? ViewState.Unseen : ViewState.Seen;
            existingView && await getRepository(View).update(existingView.id, { state : viewState });
            const viewEntity = new View();

            viewEntity.episode = await getEpisodeById(idEpisode);
            viewEntity.user = await getUserById(idUser);
            viewEntity.state = ViewState.Seen;

            const view = !existingView && await getRepository(View).save(viewEntity);

            return existingView ? res.status(200).json({
                message : 'Modification réussie'
            }) : res.status(200).json({
                view,
                message : 'ajout favoris reussi'
            });

        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }

    }

}