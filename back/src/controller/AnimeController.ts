import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import userLogged from '../../../front/reducer/userLogged';
import { Anime } from '../entity/Anime';
import { getUserAnime } from '../helper/userAnimeHelper';
import { getUserById } from '../helper/userHelpers';
import { UserAnime } from '../entity/UserAnime';
import { getAnimeById, getAllAnimes, insertAnime } from '../helper/animeHelpers';

export default class AnimeController {

    static async getAllAnimes(req : Request, res : Response) {
        try {
            let animes = await getAllAnimes();

            return res.status(200).json({
                animes
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async getAnimeById(req : Request, res : Response) {
        try {
            let id = req.params.id;
            let anime = await getAnimeById(parseInt(id));

            return res.status(200).json({
                anime
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async addToList(req : Request, res : Response) {
        const idUser = req.body.idUser;
        const idAnime = req.body.idAnime;
        const userEntity = await getUserById(parseInt(idUser));
        const animeEntity = await getAnimeById(idAnime);
        const userAnimeEntity = new UserAnime();
        try {
            userAnimeEntity.user = userEntity;
            userAnimeEntity.anime = animeEntity;

            const useranime = await getRepository(UserAnime).save(userAnimeEntity);

            return res.status(200).json({
                useranime,
                message : 'ajout favoris reussi'
            });

        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }

    }

    static async removeFromList(req : Request, res : Response) {
        const idUser = req.body.idUser;
        const idAnime = req.body.idAnime;
        try {
            const userAnime = await getUserAnime(idUser, idAnime);
            await getRepository(UserAnime).delete(userAnime.id);
            return res.status(200).json({
                message : 'Suppression réussie'
            });
        } catch (e) {
            return res.status(500).json({
                'message' : e.message
            });
        }

    }

    static async insertAnime(req : Request, res : Response) {
        const animeEntity = new Anime();
        try {
            animeEntity.title = req.body.title;
            animeEntity.description = req.body.description;
            animeEntity.releaseDay = req.body.releaseDay;
            animeEntity.releaseHour = req.body.releaseHour;
            animeEntity.episodeDuration = Number(req.body.episodeDuration);
            animeEntity.yearReleased = req.body.yearReleased;
            animeEntity.cover = req.body.cover;
            const anime = await insertAnime(animeEntity);
            return res.status(200).json({
                anime,
            });
        } catch (e) {
            console.log(e);
            res.sendStatus(500);
        }
    }

    static async updateAnime(req : Request, res : Response) {
        let id = req.params.id;
        const animeData = req.body;
        try {
            await getRepository(Anime).update(id, animeData);
            return res.status(200).json({
                message : 'Modification réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }

    static async deleteAnime(req : Request, res : Response) {
        let id = req.params.id;
        try {
            await getRepository(Anime).delete(id)
            return res.status(200).json({
                message : 'Suppression réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }

}