import * as bcrypt from 'bcrypt';
import { Request, Response } from 'express';
import * as generator from 'generate-password';
import * as nodemailer from 'nodemailer';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import { getAllUsers, getUserByCredential, getUserById, insertUser, resetPassword, verifyEmail, verifyUsername } from '../helper/userHelpers';
import moment from 'moment';

export default class UserController {

    static async getAllUsers(req : Request, res : Response) {
        try {
            const users = await getAllUsers();
            return res.status(200).json({
                users
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async getUserById(req : Request, res : Response) {
        try {
            const id = req.params.id;
            const user = await getUserById(parseInt(id));
            return res.status(200).json({
                user
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async insert(req : Request, res : Response) {
        const userEntity : User = new User();
        try {

            const getVerifiedUsername = await verifyUsername(req.body.username);
            const getVerifiedEmail = await verifyEmail(req.body.email);
            const regexEmail = new RegExp(/^[a-zA-Z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,}$/);
            if (getVerifiedUsername[0]['count'] > 0) {
                res.status(400).json({
                    message : 'Pseudo existant. Utilisez un autre pseudo svp'
                });
                return;
            }
            if (!regexEmail.test(req.body.email)) {
                console.log('bad email');
                res.status(400).json({
                    message : 'Le format de l\'email est incorrect'
                });
                return;
            }
            if (getVerifiedEmail[0]['count'] > 0) {
                res.status(400).json({
                    message : 'Email existant. Utilisez un autre email svp'
                });
                return;
            }

            userEntity.createdAt = moment().locale('fr').format('DD-MM-YYYY HH:mm:ss');

            userEntity.username = req.body.username;
            userEntity.email = req.body.email;
            userEntity.avatar = req.body.avatar;
            userEntity.password = bcrypt.hashSync(req.body.password, 10);
            const user = await insertUser(userEntity);
            return res.status(200).json({
                user,
            });
        } catch (e) {
            console.log(e);
            res.sendStatus(500);
        }
    }

    static async login(req : Request, res : Response) {
        const usernameOrEmail = req.body.username;
        const password = req.body.password;
        console.log(req.body);
        
        try {
            const user = await getUserByCredential(usernameOrEmail);
            if (user) {
                bcrypt.compare(password, user.password, function (err, result) {
                    if (result === true) {
                        req.session.loggedin = true;
                        req.session.username = usernameOrEmail;
                        res.status(200).json({
                            user : [user],
                        });
                        return;
                    } else {
                        res.status(403).json({
                            message : 'Mot de passe incorrect'
                        });
                        return;
                    }
                });
            } else {
                res.status(404).json({
                    message : 'Email ou pseudo incorrect'
                });
                return;
            }
        } catch (e) {
            console.log(e);
        }
    }

    static logout(req : Request, res : Response) {
        req.session = null;
        res.send('logout ok');
    }

    static async resetPassword(req : Request, res : Response) {
        const email = req.body.email;
        const transporter = nodemailer.createTransport({
            service : 'gmail',
            auth : {
                user : 'seb.hitema@gmail.com',
                pass : 'sebsebseb'
            }
        });

        try {
            const checkEmail = await getUserByCredential(email);
            if (checkEmail) {
                const password = generator.generate({
                    length : 10,
                    numbers : true
                });

                const mailOptions = {
                    from : 'myanime@gmail.com',
                    to : email,
                    subject : 'Nouveau mot de passe',
                    html : '<p>Voici votre nouveau mot de passe : ' + '<b>' + password + '</b>' + '</p><p>Vous pouvez le changer sur la page profil</p>'
                };

                const passwordHash = bcrypt.hashSync(password, 10);

                transporter.sendMail(mailOptions, async function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        await resetPassword(email, passwordHash);
                        res.status(200).json({
                            message : 'Envoi réussi'
                        });
                        return;
                    }
                });
            } else {
                res.status(404).json({
                    message : 'Cette adresse mail n\'éxiste pas dans la base'
                });
                return;
            }
        } catch (e) {
            console.log(e);
        }
    }

    static async update(req : Request, res : Response) {
        let id = req.params.id;
        const userData = req.body;
        try {
            await getRepository(User).update(id, userData);
            return res.status(200).json({
                message : 'Modification réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }

    static async delete(req : Request, res : Response) {
        let id = req.params.id;
        try {
            await getRepository(User).delete(id);
            return res.status(200).json({
                message : 'Suppression réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }
}