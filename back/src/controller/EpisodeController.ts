import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Episode } from '../entity/Episode';
import { getAnimeById } from '../helper/animeHelpers';
import { getEpisodeById } from '../helper/episodeHelpers';

export default class EpisodeController {

    static async getAllEpisodes(req : Request, res : Response) {
        try {
            const episodes = await getRepository(Episode)
                .createQueryBuilder()
                .select('*')
                .getRawMany();
            return res.status(200).json({
                episodes
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async getEpisodeById(req : Request, res : Response) {
        const id = req.params.id;
        try {
            let episodes = await getEpisodeById(parseInt(id));
            return res.status(200).json({
                episodes
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async insertEpisode(req : Request, res : Response) {
        const idAnime = req.body.idAnime;
        const animeEntity = await getAnimeById(parseInt(idAnime));
        const episodeEntity = new Episode();
        try {
            episodeEntity.title = req.body.title;
            episodeEntity.episodeNumber = req.body.episodeNumber;
            episodeEntity.description = req.body.description;
            episodeEntity.anime = animeEntity;
            episodeEntity.cover = req.body.cover;

            const episode = await getRepository(Episode).save(episodeEntity);

            return res.status(200).json({
                episode,
                message : 'ajout episode OK'
            });

        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

    static async updateEpisode(req : Request, res : Response) {
        const id = req.params.id;
        const episodeData = req.body;
        try {
            await getRepository(Episode).update(id, episodeData);
            return res.status(200).json({
                message : 'Modification réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }

    static async deleteEpisode(req : Request, res : Response) {
        const id = req.params.id;
        try {
            await getRepository(Episode).delete(id)
            return res.status(200).json({
                message : 'Suppression réussie'
            });
        } catch (e) {
            res.status(500).json({
                message : e.message
            });
            return;
        }
    }

}