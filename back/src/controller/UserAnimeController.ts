import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { UserAnime } from '../entity/UserAnime';

export default class UserAnimeController {

    static async getAllUserAnime(req : Request, res : Response) {
        try {
            const userAnimes = await getRepository(UserAnime)
                .createQueryBuilder()
                .select('*')
                .getRawMany();
            return res.status(200).json({
                userAnimes
            });
        } catch (e) {
            return res.status(404).json({
                'message' : e.message
            });
        }
    }

}