export enum ViewState {
    Seen = 'S',
    Unseen = 'UN',
}