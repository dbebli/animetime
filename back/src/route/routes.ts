import { Router } from 'express';
import AnimeController from '../controller/AnimeController';
import CommentController from '../controller/CommentController';
import EpisodeController from '../controller/EpisodeController';
import UserAnimeController from '../controller/UserAnimeController';
import UserController from '../controller/UserController';
import ViewController from '../controller/ViewController';

export const router = Router();

router.get('/user', UserController.getAllUsers);
router.get('/user/:id', UserController.getUserById);
router.post('/user/create', UserController.insert);
router.post('/user/login', UserController.login);
router.get('/user/logout', UserController.logout);
router.post('/user/passwordReset', UserController.resetPassword);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.delete);

router.get('/userAnime', UserAnimeController.getAllUserAnime);

router.get('/anime', AnimeController.getAllAnimes);
router.post('/anime', AnimeController.insertAnime);
router.put('/anime/:id', AnimeController.updateAnime);
router.delete('/anime/:id', AnimeController.deleteAnime);
router.post('/anime/addTolist', AnimeController.addToList);
router.post('/anime/removeFromList', AnimeController.removeFromList);

router.get('/episode', EpisodeController.getAllEpisodes);
router.post('/episode', EpisodeController.insertEpisode);
router.get('/episode/:id', EpisodeController.getEpisodeById);
router.put('/episode/:id', EpisodeController.updateEpisode);
router.delete('/episode/:id', EpisodeController.deleteEpisode);

router.get('/comment', CommentController.getAllComments);
router.post('/comment', CommentController.addComment);
router.delete('/comment/:id', CommentController.deleteComment);

router.get('/view', ViewController.getAll);
router.post('/view', ViewController.toggleView);