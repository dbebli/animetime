import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IAnime } from '../interface/IAnime';
import { Identified } from '../type/Identified';
import { UserAnime } from './UserAnime';

@Entity('anime')
export class Anime implements Identified<IAnime> {

    @PrimaryGeneratedColumn()
    id : number;

    @Column()
    title : string;

    @Column({ type : 'text' })
    description : string;

    @Column()
    releaseDay : string;

    @Column()
    releaseHour : string;

    @Column()
    yearReleased : string;

    @Column()
    episodeDuration : number;

    @Column({ type : 'longtext', nullable : true })
    cover : string;

    @OneToMany(type => UserAnime, userAnime => userAnime.anime, { onDelete : 'CASCADE' })
    public userAnime! : UserAnime[];

}
