import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IComment } from '../interface/IComment';
import { Episode } from './Episode';
import { User } from './User';

@Entity('comment')
export class Comment implements Partial<IComment> {

    @PrimaryGeneratedColumn()
    id : number;

    @Column({ type : 'text' })
    comment : string;

    @ManyToOne(type => Episode, episode => episode.id, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idEpisode' })
    public episode : Episode;

    @ManyToOne(type => User, user => user.id, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idUser' })
    public user : User;

    @Column()
    createdAt : string;

    @Column({ nullable : true })
    idCommentReference? : number;

}
