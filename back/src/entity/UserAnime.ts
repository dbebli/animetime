import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Anime } from './Anime';
import { User } from './User';

@Entity('useranime')
export class UserAnime {

    @PrimaryGeneratedColumn()
    id : number;

    @ManyToOne(type => User, user => user.userAnime, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idUser' })
    user : User;

    @ManyToOne(type => Anime, anime => anime.userAnime, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idAnime' })
    anime : Anime;
}
