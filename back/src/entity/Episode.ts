import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IEpisode } from '../interface/IEpisode';
import { Anime } from './Anime';
import { Comment } from './Comment';

@Entity('episode')
export class Episode implements Partial<IEpisode> {

    @PrimaryGeneratedColumn()
    id : number;

    @Column()
    title : string;

    @Column()
    episodeNumber : number;

    @Column({ type : 'text' })
    description : string;

    @ManyToOne(type => Anime, anime => anime.id, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idAnime' })
    anime : Anime;

    @Column({ type : 'longtext', nullable : true })
    cover : string;

    @OneToMany(type => Comment, comment => comment.episode, { onDelete : 'CASCADE' })
    public comment! : Comment[];

}
