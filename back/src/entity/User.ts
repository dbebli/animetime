import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IUser } from '../interface/IUser';
import { Identified } from '../type/Identified';
import { UserAnime } from './UserAnime';
import { Comment } from './Comment';

@Entity('user')
export class User implements Identified<IUser> {

    @PrimaryGeneratedColumn()
    id : number;

    @Column()
    username : string;

    @Column()
    email : string;

    @Column()
    createdAt : string;

    @Column()
    password : string;

    @Column({ type : 'longtext', nullable : true })
    avatar? : string;

    @OneToMany(type => UserAnime, userAnime => userAnime.user, { onDelete : 'CASCADE' })
    public userAnime! : UserAnime[];

}
