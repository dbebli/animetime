import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IView } from '../interface/IView';
import { ViewState } from '../type/ViewState';
import { Episode } from './Episode';
import { User } from './User';

@Entity('view')
export class View implements Partial<IView> {

    @PrimaryGeneratedColumn()
    id : number;

    @Column()
    state : ViewState;

    @ManyToOne(type => Episode, episode => episode.id, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idEpisode' })
    public episode : Episode;

    @ManyToOne(type => User, user => user.id, { onDelete : 'CASCADE' })
    @JoinColumn({ name : 'idUser' })
    public user : User;

}
