export interface IUser {
    username : string;
    email : string;
    password : string;
    createdAt : string;
    avatar? : string;
}