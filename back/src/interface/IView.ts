import { ViewState } from '../type/ViewState';

export interface IView {
    idEpisode : number;
    state : ViewState;
    idUser : number;
}