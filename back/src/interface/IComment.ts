export interface IComment {
    idUser : number;
    idEpisode : number;
    idCommentReference? : number;
    comment : string;
    createdAt : string;
}