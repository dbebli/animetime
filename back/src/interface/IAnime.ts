import { IContent } from './IContent';

export interface IAnime extends IContent {
    yearReleased : string;
    releaseDay : string;
    releaseHour : string;
    episodeDuration : number;
}