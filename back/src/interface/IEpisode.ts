import { IContent } from './IContent';

export interface IEpisode extends IContent {
    idAnime : number;
    episodeNumber : number;
}