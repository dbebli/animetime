export interface IContent {
    cover? : string;
    title : string;
    description : string;
}