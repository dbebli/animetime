import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import session from 'express-session';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { Anime } from './src/entity/Anime';
import { Comment } from './src/entity/Comment';
import { Episode } from './src/entity/Episode';
import { User } from './src/entity/User';
import { UserAnime } from './src/entity/UserAnime';
import { View } from './src/entity/View';
import { router } from './src/route/routes';

const app = express();
app.use(session({
    secret : 'secret',
    resave : true,
    saveUninitialized : true
}));
app.use(cors({ origin : true }));

app.use(bodyParser.json({ limit : '50mb'}));
app.use(bodyParser.urlencoded({ limit : '50mb', extended : true }));

app.use(router);

// Connexion à la base
createConnection({
    type : 'mysql',
    host : 'localhost',
    port : 3306,
    username : 'root',
    password : '',
    database : 'my_anime',
    synchronize : true,
    logging : false,
    charset : 'utf8mb4',
    entities : [
        Anime,
        Comment,
        Episode,
        User,
        UserAnime,
        View
    ]
}).then(async connection => {
    console.log('Connected to DB');
    // Connexion au serveur
    app.listen(3022, () => {
        console.log('Server listening on port 3022');
    });
}).catch(error => console.log('TypeORM connection error: ', error));

export default app;